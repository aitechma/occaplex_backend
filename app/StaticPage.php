<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
class StaticPage extends Model
{
    //
    use HasSlug;

    protected $fillable = ['name', 'slug', 'header', 'sub_header', 'excerpt', 'section1_content', 'section1_image', 'section2_content', 'section2_image', 'section3_content', 'section3_image'];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')
            ->usingSeparator('-')
            ->slugsShouldBeNoLongerThan(80)
            ->doNotGenerateSlugsOnUpdate();
    }
}
