<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor_payment_method extends Model
{
    //
    protected $fillable = ['vendor', 'payment_method', 'status', 'created_at', 'updated_at', 'paypal_id', 'stripe_id', 'bank_name', 'bank_number', 'bank_sort_code', 'account_name', 'bank_verification_number'];
}
