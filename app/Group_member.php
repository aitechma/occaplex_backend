<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group_member extends Model
{
    //
    protected $fillable = [
        "email",	"approved",	"user",	"group", "removed", "role", "event_id"];
}
