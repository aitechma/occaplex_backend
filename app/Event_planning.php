<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Event_planning extends Model
{
    //
    protected $table = 'event_planning';
    
    protected $primaryKey = 'uuid';
    
    protected $fillable = ['user_id','plan_as_group','do_it','event_type', 'date',	'address','region','city','country','population',	'event_theme','primary_color', 'secondary_color',	'event_checklist',	'planning_group', 'event_activities','privacy', 'conversation_group',	'relative_event', 'paid', 'status', 'time', 'event_title'];
    
    protected $casts = [
        'event_checklist' => 'array',
        // 'planning_group' => 'array'
        
    ];
}
