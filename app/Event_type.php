<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Event_type extends Model
{
    use HasSlug;
    //
    protected $primaryKey = 'uuid';
    
    protected $fillable = [
        'event_type', 'image', 'color', 'excerpt', 'parent', 'relative_category'    
    ];
    
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('event_type')
            ->saveSlugsTo('slug')
            ->usingSeparator('-')
            ->slugsShouldBeNoLongerThan(50)
            ->doNotGenerateSlugsOnUpdate();
    }

    protected $casts = [
        
    ];

}
