<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
// use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Group extends Model
{
    //
    use HasSlug;
    //
    protected $primaryKey = 'uuid';
    
    protected $fillable = [
        'creator', 'event_id', 'slug', 'title', 'description', 'cover_photo', 'status', 'deleted'    
    ];
    
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingSeparator('-')
            ->slugsShouldBeNoLongerThan(50)
            ->doNotGenerateSlugsOnUpdate();
    }

    protected $casts = [
        
    ];
}
