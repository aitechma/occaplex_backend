<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricing_package extends Model
{
    //
    protected $fillable = [
        'day_needed', 'faster_amount', 'flavours', 'title', 'service', 'vendor', 'status', 'description', 'price'
    ];
}
