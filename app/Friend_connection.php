<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend_connection extends Model
{
    //
    protected $fillable = ['sender',	'friend',	'accepted',	'blocked'];
}
