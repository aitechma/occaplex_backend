<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable = ['uuid', 'ref', 'gref', 'product', 'quantity', 'listing_type', 'plan', 'sales_price', 'checkout_fee', 'tax', 'shipping_fee', 'total_price', 'vendor_revenue', 'vendor_charge', 'vendor_uuid', 'user_uuid', 'product_uuid', 'order_status', 'payment_status', 'payment_method', 'payment_ref_id', 'order_meta', 'event_data', 'associated_event_uuid', 'title', 'payment_data', 'image'];

    protected $casts = [
        'product' => 'array',
        'order_meta' => 'array',
        'event_data' => 'array',
        'payment_data' => 'array',
        'created_at' => 'datetime:Y-m-d'
        
    ];
}
