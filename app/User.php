<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use HasApiTokens, Notifiable;
    
    // protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    protected $fillable = [
        'uuid', 'name', 'email', 'password', 'username', 'phone', 'country', 'country_code', 'city', 'zipcode', 'business_name', 'address', 'about_me', 'about_service', 'facebook', 'twitter', 'youtube', 'instagram', 'website', 'service_category', 'email_verify_token', 'email_verify_expiration', 'phone_verify_token', 'phone_verify_expiration','facebook_id','facebook_id_token','google_id','google_id_token', 'picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'service_category' => 'array'
    ];
}
