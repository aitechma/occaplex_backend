<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_supplier extends Model
{
    //
    protected $fillable = [
        "event_id",	"user_id", "group_id", "listing_id", "status", "listing_purchased", "listing_purchased_by", "event_supplier_features"];

        protected $casts = [
            'event_supplier_features' => 'array'
            
        ];
}
