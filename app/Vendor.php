<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    protected $primaryKey = 'uuid';
    
    protected $fillable = [
        "user_id","username","business_name",
	"address",
	"country",
	"city",
	"zipcode",
	"phone",
	"business_email",
	"business_logo",
	"website",
	"twitter",
	"facebook",
	"instagram",
	"youtube",
	"about_service",
	"service_category",
	"business_proof",
	"utility_bill",
	"government_id"
		];
		
	
	protected $casts = [
		'created_at' => 'datetime'
	];
}
