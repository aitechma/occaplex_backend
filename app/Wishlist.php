<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Wishlist extends Model
{
    //
    protected $primaryKey = 'uuid';
    
    protected $fillable = ['user_id','listing_id','status','product_type'];
}
