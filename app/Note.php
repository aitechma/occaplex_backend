<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
    	'order_uuid','user_uuid','user_type','note','media'
    ];
}
