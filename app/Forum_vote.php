<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum_vote extends Model
{
    //
    protected $fillable = ["forum", "answer", "user"];
}
