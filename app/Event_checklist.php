<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_checklist extends Model
{
    //
    protected $fillable = [
        "event_id",	"user_id",	"category_id",	"vendor_id", "group_id", "budget",	"status"];
}
