<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_requirement extends Model
{
    //
    protected $fillable = [
        'requirement', 'service', 'vendor', 'status'
    ];
    
    protected $table = 'product_requirement';
}
