<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_gallery extends Model
{
    //
    
    protected $fillable = [
        'media_title', 'media_url', 'media_type', 'service', 'vendor', 'status'
    ];
    
    protected $table = 'product_gallery';
}
