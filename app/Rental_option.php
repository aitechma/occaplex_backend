<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Rental_option extends Model
{
    //
    protected $primaryKey = 'uuid';
    
    protected $fillable = [
        'option'   
    ];
}
