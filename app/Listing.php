<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Listing extends Model
{
    use HasSlug;
    //
    protected $primaryKey = 'uuid';
    
     protected $fillable = [
        'title', 'category', 'service_type', 'tags', 'description', 'total_price', 'country', 'city', 'region', 'verification', 'vendor', 'status','pricing_package', 'requirements', 'faq', 'gallery', 'service_type', 'listing_type','deleted', 'sku', 'quantity_per_unit', 'unit_price', 'size', 'color', 'discount', 'stock', 'available', 'note', 'shipping', 'min_price', 'max_price', 'unit_option', 'width', 'height', 'weight', 'length', 'featured', 'additional_fee_name', 'additional_fee', 'attributes', 'additional_fee_optional'
    ];
    
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingSeparator('-')
            ->slugsShouldBeNoLongerThan(50)
            ->doNotGenerateSlugsOnUpdate();
    }
    
    protected $casts = [
        'tags' => 'array',
        'faq' => 'array',
        'requirements' => 'array',
        'pricing_package' => 'array',
        'attributes' => 'array',
        'gallery' => 'array',
        'service_type' => 'array',
        'discount' => 'integer',
        'total_price' => 'integer'
        
    ];
    
    
}
