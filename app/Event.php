<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Event extends Model
{
    //
    use HasSlug;
    
    protected $primaryKey = 'uuid';
    
     protected $fillable = ['title',	'location',	'description',	'image',	'status',	'price',	'capacity',	'date',	'event_planning', 'slug', 'time'];
     
     public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingSeparator('-')
            ->slugsShouldBeNoLongerThan(50)
            ->doNotGenerateSlugsOnUpdate();
    }
}
