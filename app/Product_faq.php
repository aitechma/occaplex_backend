<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_faq extends Model
{
    //
    protected $fillable = [
        'question', 'answer', 'service', 'vendor', 'status'
    ];
}
