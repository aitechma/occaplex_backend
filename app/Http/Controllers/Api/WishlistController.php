<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Listing;
use App\Media;
use Validator;
use Illuminate\Support\Facades\Auth; 
use App\Wishlist;

class WishlistController extends Controller
{
    //
    
    public function save(Request $request) {
        $data = $request->all();
        
        $validator = Validator::make($request->all(), [ 
            'listing_id' => 'required'
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);  
        }
        
        $user = Auth::user();
        
        $data['user_id'] = $user['uuid'];
        
        $data['status'] = 'open';
        
        if(Wishlist::where('listing_id', $data['listing_id'])->where('user_id', $data['user_id'])->count() > 0) {
            return response()->json([
            'status' => 'failed',
            'message' => 'Error occurred product already added'
            ], 401);
        }
        
        if(Wishlist::create($data)) {
            return response()->json([
            'status' => 'success',
            'message' => 'Wishlist saved successfully'
            ], 200);
        } else {
            return response()->json([
            'status' => 'failed',
            'message' => 'Error occurred while saving wishlist'
            ], 401);
        }
    }
    
    public function get() {
        $user = Auth::user();
        
        $wishlist = Wishlist::where('user_id', $user['uuid'])->get();
        
        $chunk = [];
        
        foreach($wishlist as $wish) {
            $wish['product_details'] = Listing::where('uuid', $wish['listing_id'])->first();
            // $wish['product_details']['gallery'] = Media::where('content_type', 'product_image_'.$wish['product_details']['uuid'])->pluck('url')->first();
            
            array_push($chunk, $wish);
        }
        
        
        
        return response()->json([
            'status' => 'success',
            'data' => $chunk,
            'message' => 'My wishlists'
        ], 200);
    }
    
    public function remove($id) {
        $user = Auth::user();
        
        Wishlist::where([['uuid', $id],['user_id', $user['uuid']]])->delete();
        
        return response()->json([
        'status' => 'success',
        'message' => 'Wishlist deleted successfully'
        ], 200);
    }
}
