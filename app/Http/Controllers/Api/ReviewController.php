<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Review;
use App\Listing;
use Str;
use Validator;

class ReviewController extends Controller
{
    public function addReview(Request $request)
    {
    	$user = auth()->user();
    	$data = $request->all();

    	if(Review::where([
    		['user',$user['uuid']],
    		['service',$data['product_uuid']]
    	])->exists())
    	{
    		return response()->json([
    			'message' => 'You have reviewed this product before'
    		]);
    	}else
    	{
    		$validator = Validator::make($request->all(),[
    		'rating' => 'required',
    		'product_uuid' => 'required',
    	]);

    	if ($validator->fails()) 
    	{ 
            return response()->json(['error'=>$validator->errors()], 401);  
        }

        $product = Listing::where('uuid',$data['product_uuid'])->first();

        $review = new Review;
    	$review->uuid = Str::uuid();
    	$review->content = $data['content'];
    	$review->rating = $data['rating'];
    	$review->service = $data['product_uuid'];
    	$review->vendor = $product['vendor'];
    	// $review->communication_level = ($data['communication_level'] == 1 ? 'fair' : $data['communication_level'] == 2 ? 'good' : $data['communication_level'] == 3 ? 'excellent' ) : $data['communication_level'] == 0 ? 'poor' :null ;
    	$review->deliver_on_time = $data['deliver_on_time'];
    	$review->recommend_to_friend = $data['recommend'];
    	$review->status = 'pending';
    	$review->user = $user['uuid'];
    	$review->user_name = is_null($data['user_name']) ?  $data['user_name'] : null;
        if($data['communication_level'] == 1)
        {
        	$review->communication_level = 'fair';
        }elseif($data['communication_level'] == 2)
        {
        	$review->communication_level = 'good';
        }elseif($data['communication_level'] == 3)
        {
        	$review->communication_level = 'excellent';
        }elseif($data['communication_level'] == 0)
        {
        	$review->communication_level = 'poor';
        }

        $review->save();

        return response()->json([
        	'data' => $review
        ],200);
    	}
    }

    public function getReviewByService($service)
    {
    	if(Review::where('service',$service)->exists())
    	{
    		$reviews = Review::where('service',$service)->get();

    		$_review = [];

    		foreach ($reviews as $review) {
    			$user_image = User::where('uuid',$review['user'])->pluck('avatar');
    			$review['user_image'] = $user_image;

    			$_reviewe[] = $review;
    		}
    		return response()->json([
    				'data' => $_reviewe
    			],200);
    		

    		//$reviews['user_image'] = User::where('uuid',$userArray)->pluck('avatar');
    	}else
    	{
    		return response()->json([
    			'message' => 'Reviews not available'
    		],404);
    	}
    }

    public function getVendorsReview($vendorUuid)
    {
    	if(Review::where('vendor',$vendorUuid)->exists())
    	{
    		$reviews =  Review::where('vendor',$vendorUuid)->get();

    		return response()->json([
    			'message' => 'Vendors Review',
    			'data' => $reviews
    		],200);
    	}else
    	{
    		return response()->json([
    			'message' => 'You have no review'
    		],404);
    	}
    }

    public function getMyReviews()
    {
    	$user = auth()->user();

    	if(Review::where('user',$user['uuid'])->exists())
    	{
    		$reviews = Review::where('user',$user['uuid'])->get();

    		return response()->json([
    			'message' => 'My reviews',
    			'data' => $reviews
    		],200);

    	}else
    	{
    		return response()->json([
    			'message' => 'You have no review yet'
    		],404);
    	}
    }
}
