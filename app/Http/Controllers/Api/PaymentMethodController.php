<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vendor;
use App\Vendor_payment_method;
use Validator;

class PaymentMethodController extends Controller
{
    //
    public function getPayMethod() {
        $user = Auth::user();

        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();

        $data = Vendor_payment_method::where('vendor', $vendor)->first();

        return response()->json([
            'status' => 'success',
            'message' => 'Payment method retrieved successfully',
            'data' => $data
        ], 200);
    }

    public function updatePaymentMethod(Request $request) {

        $data = $request->all();

        $user = Auth::user();

        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();

        $data['vendor'] = $vendor;

        if(Vendor_payment_method::where('vendor', $vendor)->exists()) {
            Vendor_payment_method::where('vendor', $vendor)->update($data);
            return response()->json([
                'status' => 'success',
                'message' => 'Payment method updated successfully',
                'data' => $data
            ], 200);
        } else {
            Vendor_payment_method::create($data);
            return response()->json([
                'status' => 'success',
                'message' => 'Payment method created successfully',
                'data' => $data
            ], 200);
        }


    }
}
