<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Http\Request;
use App\User;
use App\Message;
use App\Contact;
use Validator;

class MessageController extends Controller
{
    //
    public function myMessages($contact) {
        $user = Auth::user();
        
        
        $messages = Message::where([['sender', '=', $user['uuid']],['recipient', '=',$contact]])->orWhere([['recipient', '=', $user['uuid']],['sender', '=',$contact]])->get();
        
        //Update read status
        
        Message::where([['sender', '=', $user['uuid']],['recipient', '=',$contact]])->orWhere([['recipient', '=', $user['uuid']],['sender', '=',$contact]])->update(['read'=> 1]);
        
        
        return response()->json([
                'status' => 'success',
                'message' => 'Message sent successfully',
                'data' => $messages
          ], 200);
        
        
        
    }
    
    public function sendMessage(Request $request) {
        $user = Auth::user();
        
        $data = $request->all();
        
        $data['sender'] = $user['uuid'];
        
        if(Contact::where('user', $user['uuid'])->where('contact', $data['recipient'])->count() > 0) {
            Message::create($data);
        
            return response()->json([
                    'status' => 'success',
                    'message' => 'Message sent successfully'
               ], 200);
        } else {
            $contact_data = Array('user' => $user['uuid'], 'contact' => $data['recipient']);
            Contact::create($contact_data);
            Message::create($data);
        
        return response()->json([
                'status' => 'success',
                'message' => 'Message sent successfully'
           ], 200);
        }
        
        
        
    }
    
    public function myContacts() {
        $user = Auth::user();
        
        $contacts = [];
        
        $allContacts = Contact::where('user',$user['uuid'])->orWhere('contact', $user['uuid'])->get();
        
        foreach($allContacts as $allContact) {
            $contact = $allContact['user'] == $user['uuid'] ? $allContact['contact'] : $allContact['user'];
            
            array_push($contacts, $contact);
        }
        
        $results = [];
        
        foreach($contacts as $contact_id) {
            $contact_data = User::where('uuid', $contact_id)->first();
            
            array_push($results, $contact_data);
            
        }
        
        return response()->json([
                'status' => 'success',
                'message' => 'Contacts retrieved successfully',
                'data' => $results
           ], 200);
    }
}
