<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Forum;
use App\Forum_answer;
use App\Forum_vote;
use App\Category;
use Validator;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    //

    public function createTopic(Request $request) {
        $validator = Validator::make($request->all(), [ 
            'topic' => 'required', 
            'description' => 'required', 
            'category' => 'required', 
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);  
        }

        $user = Auth::user();

        $data = $request->all();

        $data['author'] = $user['uuid'];

        if(Forum::create($data)) {
            return response()->json([
                'status' => 'success',
                'message' => 'Topic created'
        ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Topic canot be created'
        ], 200);
        }




    }


    public function getTopics($type) {

        switch ($type) {
            case 'all':
                # code...
                $topics = Forum::get();
                foreach ($topics as $topic) {
                    $topic['author'] = User::where('uuid', $topic['author'])->first();
                    $topic['answers'] = Forum_answer::where('forum', $topic['id'])->count();
                    $topic['category'] = Category::where('uuid', $topic['category'])->pluck('name')->first();
                }
                break;
            
            default:
                # code...
                $topics = Forum::where('category', $type)->get();
                foreach ($topics as $topic) {
                    $topic['author'] = User::where('uuid', $topic['author'])->first();
                    $topic['answers'] = Forum_answer::where('forum', $topic['id'])->count();
                }
                break;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Topics retrieved',
            'data' => $topics
        ], 200);
        
        
    }

    public function getTopicDetails($id) {
        $details = Forum::where('id', $id)->first();
        return response()->json([
            'status' => 'success',
            'message' => 'Topics details',
            'data' => $details
        ], 200);

    }



    public function answerQuestion(Request $request) {
        $validator = Validator::make($request->all(), [ 
            'forum' => 'required', 
            'answer' => 'required',
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);  
        }
        $user = Auth::user();

        $data = $request->all();

        $data['user'] = $user['uuid'];

        if(Forum_answer::create($data)) {
            return response()->json([
                'status' => 'success',
                'message' => 'Answer submitted'
        ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Answer canot be submitted'
        ], 200);
        }


    }


    public function getTopicAnswers($topic) {

        $answers = Forum_answer::where('forum', $topic)->get();
        return response()->json([
            'status' => 'success',
            'message' => 'Answers retrieved',
            'data' => $answers
        ], 200);


    }

    public function voteAnswer($type, $forum, $answer) {
        $user = Auth::user();
        if(Forum::where('id', $forum)->exists() && Forum_answer::where('id', $answer)->exists()) {
            switch ($type) {
            case 'vote':
                # code...
                $data = ['forum' => $forum, 'answer' => $answer, 'user' => $user['uuid']];
                if(Forum_vote::where('forum', $forum)->where('answer', $answer)->where('user', $user['uuid'])->count() > 0) {
                    return response()->json([
                        'status' => 'invalid',
                        'message' => 'Already voted'
                    ], 200);
                } else {
                    Forum_vote::create($data);
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Vote successful'
                    ], 200);

                }
                break;
            
            default:
                # code...
                Forum_vote::where('forum', $forum)->where('answer', $answer)->where('user', $user['uuid'])->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => 'unvote successful'
                ], 200);
                break;
        }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Answer does not exists'
            ], 200);
        }
        
    }
}
