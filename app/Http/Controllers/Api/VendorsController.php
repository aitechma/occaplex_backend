<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str; 
use DB;
use App\User;
use App\Vendor;
use App\Media;
use App\Calendar;
use App\Listing;
use App\Order;
use JD\Cloudder\Facades\Cloudder;
use App\Return_policy;
use Validator;
use Igaster\LaravelCities\Geo;

class VendorsController extends Controller
{
    //
    public function becomeVendor(Request $request) {
        $data = $request->all();
        
        $user = Auth::user();
        
        if(Vendor::where('user_id', $user['uuid'])->count() > 0) {
            return response()->json([
            'status' => 'error',
            'message' => 'The user is already a vendor'
         ], 400);   
        } 
        
        if(Vendor::where('user_id', $user['uuid'])->where('business_confirmed', false)->count() > 0) {
            return response()->json([
            'status' => 'error',
            'message' => 'This user has a pending vendor application'
         ], 400);   
        } 
        
        if(Vendor::where('user_id', $user['uuid'])->where('business_confirmed', true)->count() > 0) {
            return response()->json([
            'status' => 'error',
            'message' => 'This user is already a vendor'
         ], 400);   
        } 
        
        $data['business_confirmed'] = false;
        $data['user_id'] = $user['uuid'];
        $data['service_category'] = implode(', ', $data['service_category']);
        
        
        $data['username'] = preg_replace("/[^a-zA-Z]/", "", $data['business_name']);
        
        $options = array();
        
        if($request->has('business_logo')) {

            if(Cloudder::upload($data['business_logo'], $options)) {
                $upload_result = Cloudder::getResult();
                
                $data['business_logo'] = $upload_result['secure_url'];
            
            };
            
        }

        if(Cloudder::upload($data['government_id'], $options)) {
            $upload_result = Cloudder::getResult();
            
            $data['government_id'] = $upload_result['secure_url'];
        
        };

        if($request->has('utility_bill')) {
            $data['utility_bill'] = 'Nil';
        }

        if(Cloudder::upload($data['business_proof'], $options)) {
            $upload_result = Cloudder::getResult();
            
            $data['business_proof'] = $upload_result['secure_url'];
        
        };
        
        // dd($data);
        if(Vendor::create($data)) {
            $user_update = Array(
                'business_confirmed' => '1'
            );
            
            User::where('uuid', $user['uuid'])->update($user_update);

            // Send email

            $vendorDat = Array('business_name' => $data['business_name'], 'name' => $user['name']);

            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('emails.vendor_apply', $vendorDat, function($message) use ($user)
            {
                $message
                    ->from('info@aitechma.com', 'Occaplex')
                    ->to($user['email'], $user['name'])
                    ->subject('New Occaplex Vendor Application!');
            });


            return response()->json([
            'status' => 'success',
            'message' => 'Vendor application submitted successfully',
            'user' => $user['uuid'],
            'vendor' => Vendor::where('user_id', $user['uuid'])->get()
         ], 200); 
            
        }
        
        else {
            return response()->json([
            'status' => 'failed',
            'message' => 'Error occurred while sending application.'
         ], 401);
        }
    }
    
    public function businessDetails() {
        $user = Auth::user();
        
        if(Vendor::where('user_id', $user['uuid'])->count() < 1) {
            return response()->json([
            'status' => 'failed',
            'message' => 'User is not a vendor'
         ], 401);
        }
        
        $details = Vendor::where('user_id', $user['uuid'])->first();
        // $details['business_logo'] = Media::where('id', $details['business_logo'])->pluck('url')->first();
        $details['service_category'] = str_replace(' ','',explode(',', $details['service_category']));

        $details['country'] = (int)$details['country'];

        // foreach(Geo::getByIds([$details['country']]) as $country){
        //     $details['country'] = $country['name'];
        // }

        return response()->json([
            'status' => 'success',
            'message' => 'Vendor business details',
            'user' => $user['uuid'],
            'data' => $details
         ], 200); 
        
    }
    
    public function viewBusinessDetails($id) {
        // $user['uuid'] = Auth::user();
        
        // if(Vendor::where('uuid', $id)->count() < 1) {
        //     return response()->json([
        //     'status' => 'failed',
        //     'message' => 'Vendor do not exist.'
        //  ], 401);
        // }

        if(Vendor::where('username', $id)->orWhere('uuid', $id)->count() < 1) {
            return response()->json([
            'status' => 'failed',
            'message' => 'Vendor do not exist.'
         ], 401);
        }
        
        $details = Vendor::where('uuid', $id)->orWhere('username', $id)->first();
        $details['business_logo'] = Media::where('id', $details['business_logo'])->pluck('url')->first();
        
        return response()->json([
            'status' => 'success',
            'message' => 'Vendor business details',
            'data' => $details
         ], 200); 
        
    }
    
    public function updateBusinessDetails(Request $request) {
        
        $data = $request->all();
        $user = Auth::user();
        
        if(Vendor::where('user_id', $user['uuid'])->count() < 1) {
            return response()->json([
            'status' => 'failed',
            'message' => 'User is not a vendor'
         ], 401);
        }
        
        $details = Vendor::where('user_id', $user['uuid'])->update($data);
        $vendor_status = Vendor::where('user_id', $user['uuid'])->pluck('business_confirmed')->first();
        /*
        commenting business confirmed status
        
        if( $vendor_status != 1 || $vendor_status != 3) {
            Vendor::where('user_id', $user['uuid'])->update(['business_confirmed' => 0]);
        }
        */
        
        return response()->json([
            'status' => 'success',
            'message' => 'Vendor business details updated',
            'user' => $user['uuid'],
            'data' => Vendor::where('user_id', $user['uuid'])->first()
         ], 200); 
        
    }
    
    public function updateBusinessLogo(Request $request) {
        
        $data = $request->all();
        $user = Auth::user();
        
        if(Vendor::where('user_id', $user['uuid'])->count() < 1) {
            return response()->json([
            'status' => 'failed',
            'message' => 'User is not a vendor'
         ], 401);
        }
        
        if($data['business_logo']) {
            $options = array(
            );
        
            if(Cloudder::upload($data['business_logo'], $options)) {
                $upload_result = Cloudder::getResult();
                
                $data['business_logo'] = $upload_result['secure_url'];
            
            };
        }
        
        $details = Vendor::where('user_id', $user['uuid'])->update($data);

        $vendor_status = Vendor::where('user_id', $user['uuid'])->pluck('business_confirmed')->first();
        if( $vendor_status != 1 || $vendor_status != 3) {
            Vendor::where('user_id', $user['uuid'])->update(['business_confirmed' => 0]);
        }
        
        return response()->json([
            'status' => 'success',
            'message' => 'Vendor business logo updated',
            'user' => $user['uuid'],
            'data' => Vendor::where('user_id', $user['uuid'])->first()
         ], 200); 
        
    }

    public function updateBusinessDoc(Request $request, $type) {
        
        $data = $request->all();
        $user = Auth::user();
        
        if(Vendor::where('user_id', $user['uuid'])->count() < 1) {
            return response()->json([
            'status' => 'failed',
            'message' => 'User is not a vendor'
         ], 401);
        }
        
        if($data['file']) {
            $options = array(
            );
        
            if(Cloudder::upload($data['file'], $options)) {
                $upload_result = Cloudder::getResult();
                
                $media = $upload_result['secure_url'];
            
            };
        }

        $input = [];

        switch ($type) {
            case 'business_proof':
                # code...
                $input['business_proof'] = $media;
                break;
            case 'government_id':
                # code...
                $input['government_id'] = $media;
                break;
            
            default:
                # code...
                break;
        }
        
        Vendor::where('user_id', $user['uuid'])->update($input);
        $vendor_status = Vendor::where('user_id', $user['uuid'])->pluck('business_confirmed')->first();
        if( $vendor_status != 1 || $vendor_status != 3) {
            Vendor::where('user_id', $user['uuid'])->update(['business_confirmed' => 0]);
        }
        
        return response()->json([
            'status' => 'success',
            'message' => 'Vendor '.$type.' updated'
         ], 200); 
        
    }
    
    public function deleteBusiness() {
        
    }
    
    public function updateCancelPolicy(Request $request) {
        
        $data = $request->all();
        $user = Auth::user();
        
        $data['user'] = $user['uuid'];
        $data['vendor'] = Vendor::where('user_id', $data['user'])->pluck('uuid')->first();
        
        if(Return_policy::where('vendor', $data['vendor'])->count() > 0) {
            Return_policy::where('vendor', $data['vendor'])->update($data);
            
            return response()->json([
            'status' => 'success',
            'message' => 'Vendor cancellation policy updated',
            'data' => Return_policy::where('vendor', $data['vendor'])->first()
         ], 200); 
        } else {
            $policy = Return_policy::create($data);
            
            return response()->json([
            'status' => 'success',
            'message' => 'Vendor cancellation policy created',
            'data' => Return_policy::where('vendor', $data['vendor'])->first()
         ], 200); 
        }
    }
    
    public function getCancelPolicy() {
        $user = Auth::user();
        
        $data['user'] = $user['uuid'];
        $data['vendor'] = Vendor::where('user_id', $data['user'])->pluck('uuid')->first();
        
        $return_policy = Return_policy::where('vendor', $data['vendor'])->first();
        
        return response()->json([
            'status' => 'success',
            'message' => 'Vendor cancellation policy retrieved',
            'data' => Return_policy::where('vendor', $data['vendor'])->first()
         ], 200);
    }

    public function getUnavailableDates()
    {
        $vendor = Auth::user();
        if(Vendor::where('user_id',$vendor['uuid'])->exists())
        {
            $unavailable_date = Calendar::where('vendor',$vendor['uuid'])->get();
            if(count($unavailable_date) > 0)
            {
                return response()->json([
                    'message' => 'These are your unavailable dates',
                    'data' => $unavailable_date
                ],202);
            }else
            {
                return response()->json([
                    'message' => 'You have no unavailable dates'
                ],200);
            }
        }else
        {
            return response()->json([
                'message' => 'This vendor doesnt exist'
            ],404);
        }
    }

    public function addUnavailableDate(Request $request)
    {
        $vendor = Auth::user();
        $data = $request->all();

        $validator = Validator::make($data,[
            'unavailable_date' => 'required|date',
            'note' => 'required'
        ]);
        if($validator->fails())
        {
            return response([
                'error' => $validator->errors(),
                'Validation errors'
            ]);
        }
        if(Vendor::where('user_id', $vendor['uuid'])->exists())
        {
            // $data['vendor'] = $vendor['uuid'];
            // $data['uuid'] = Str::uuid();
            // $unavailableDate = Calendar::create($data);

            $unavailableDate = new Calendar;
            $unavailableDate->vendor = $vendor['uuid'];
            $unavailableDate->uuid = Str::uuid();
            $unavailableDate->note = $data['note'];
            $unavailableDate->unavailable_date = $data['unavailable_date'];
            $unavailableDate->save();

            return response()->json([
                'message' => 'Unavailable date set',
                'data' => $data
            ],202);
        }else
        {
            return response()->json([
                'message' => 'You cant perform this action'
            ],200);
        }
    }

    public function deleteUnavailableDate($id)
    {
        // $vendor = Auth::user();
        if(Calendar::where('uuid',$id)->exists()){
            $unavailable_date = Calendar::where('uuid',$id);
            $unavailable_date->delete();

            return response()->json([
                'message' => 'Date has been removed from unavailable'
            ],202);
        }
    }

    public function editUnavailableDate(Request $request,$id)
    {

        if(Calendar::where('uuid',$id)->exists())
        {
            $unavailable_date = Calendar::find($id);
            $unavailable_date->note = is_null($request->note) ? $unavailable_date->note : $request->note;

            $unavailable_date->save();

            return response()->json([
                'message' => 'Note updated successfully'
            ],200);
        }else
        {
            return response()->json([
                'message' => 'This date doesnt exist'
            ],404);
        }
    }

    public function checkUnavailableDate(Request $request){
        
        $vendor = Auth::user();
        $checks = [
            'vendor' => $vendor['uuid'],
            'unavailable_date' => $request->date
          ];

        if(Calendar::where($checks)->exists())
        {
            return response()->json([
            'message' => 'Unavailable date'
            ],202);
        }else
        {
            return response()->json([
                'message' => 'Available date'
            ],202);
        }   
        // $vendorsCalendar = Calendar::where('vendor',$vendor['uuid']);
        // if(Calendar::where('unavailable_date','>=', $vendorsCalendar['date'])->exists())
        // {
        //     return response()->json([
        //         'message' => 'Unavailable date'
        //     ],202);
        // }else
        // {
        //     return response()->json([
        //         'message' => 'Available on this date'
        //     ],202);
        // }
        //DB::table('calendars')->whereDate('unavailable_date',$request->date)->where('vendor',$vendor['uuid']->exists()      
    }

    public function vendorAnalytics(Request $request)
    {
        $vendor = Auth::user();

        $date = $request->all();
        
        if($request->has('from_date'))
        {
            if(Vendor::where('user_id',$vendor['uuid'])->exists())
            {
                $vendor = Vendor::where('user_id',$vendor['uuid'])->first();

                    $total_listings = Listing::where('vendor',$vendor['uuid'])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();

                    $total_orders = Order::where('vendor_uuid',$vendor['uuid'])->count();
                    $new_orders = Order::where([
                        ['vendor_uuid',$vendor['uuid']],
                        ['order_status','submitted']
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                    $active_orders = Order::where([
                        ['vendor_uuid',$vendor['uuid']],
                        ['order_status','processing']
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                    $completed_orders = Order::where([
                        ['vendor_uuid',$vendor['uuid']],
                        ['order_status','completed']
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                    $approved_listings = Listing::where([
                        ['vendor',$vendor['uuid']],
                        ['status',1]
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();

                    $total_revenue = Order::where([
                        ['vendor_uuid',$vendor['uuid']],
                        ['order_status','completed']
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->sum('vendor_revenue');

                    $pending_revenue = Order::where([
                        ['vendor_uuid',$vendor['uuid']],
                        ['order_status','submitted'],
                        //['order_status','processing'],
                    ])->orWhere('order_status','processing')->whereBetween('created_at',[$date['from_date'],$date['to_date']])->sum('vendor_revenue');

                    return response()->json([
                    'message' => 'Vendor Analytics betwwen '.$date['from_date'] . ' and '. $date['to_date'], 
                    'Listings' => $total_listings,
                    'Orders' => $total_orders,
                    'New Orders' => $new_orders,
                    'Active Orders' => $active_orders,
                    'Completed Orders' => $completed_orders,
                    'Approved Listings' => $approved_listings,
                    'Total Revenue' => $total_revenue,
                    'Pending Revenue' => $pending_revenue
                    ],200);
            }
        } 
        if(Vendor::where('user_id',$vendor['uuid'])->exists())
        {
            $vendor = Vendor::where('user_id',$vendor['uuid'])->first();
            $total_listings = Listing::where('vendor',$vendor['uuid'])->count();
            $total_orders = Order::where('vendor_uuid',$vendor['uuid'])->count();
            $new_orders = Order::where([
                ['vendor_uuid',$vendor['uuid']],
                ['order_status','submitted']
            ])->count();
            $active_orders = Order::where([
                ['vendor_uuid',$vendor['uuid']],
                ['order_status','processing']
            ])->count();
            $completed_orders = Order::where([
                ['vendor_uuid',$vendor['uuid']],
                ['order_status','completed']
            ])->count();
            $approved_listings = Listing::where([
                ['vendor',$vendor['uuid']],
                ['status',1]
            ])->count();

            $total_revenue = Order::where([
                ['vendor_uuid',$vendor['uuid']],
                ['order_status','completed']
            ])->sum('vendor_revenue');

            $pending_revenue = Order::where([
                ['vendor_uuid',$vendor['uuid']],
                ['order_status','submitted'],
                //['order_status','processing'],
            ])->orWhere('order_status','processing')->sum('vendor_revenue');

            return response()->json([
                'Listings' => $total_listings,
                'Orders' => $total_orders,
                'New Orders' => $new_orders,
                'Active Orders' => $active_orders,
                'Completed Orders' => $completed_orders,
                'Approved Listings' => $approved_listings,
                'Total Revenue' => $total_revenue,
                'Pending Revenue' => $pending_revenue
            ],200);
        }  
    
    }
}

