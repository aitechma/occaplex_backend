<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Vendor;
use Illuminate\Support\Str;
use App\Friend_connection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use JD\Cloudder\Facades\Cloudder;
use GuzzleHttp\Client;

class UsersController extends Controller
{
    //

    public function socialAuth(Request $request, $provider) {

        $data = $request->all();

        

        if(User::where('email', $data['email'])->exists()) { // Log the user in
            switch ($provider) {
                case 'FACEBOOK':
                    # code...
                    User::where('email', $data['email'])->update(['facebook_id'=> $data['id'], 'facebook_id_token' => $data['authToken']]);
                    break;
    
                case 'GOOGLE':
                    # code...
                    User::where('email', $data['email'])->update(['google_id'=> $data['id'], 'google_id_token' => $data['authToken']]);
                    break;
                
                default:
                    # code...
                    break;
            }

            User::where('email', $data['email'])->update(['email_verified'=> 1]);

            $user = User::where('email', $data['email'])->first();

            $token = $user->createToken('Occaplex')->accessToken; 

            return response()->json([
                'status' => 'success',
                'token' => $token,
                'message' => 'User login successful',
                'data' => $user->where('email', $data['email'])->first()
                ], 200); 
            
        }

        if(User::where('email', $data['email'])->doesntExist()) { // Create an account for the user

            $val['uuid'] = Str::uuid();
            $val['password'] = bcrypt($data['id']);
            $val['name'] = $data['name'];
            $val['email'] = $data['email'];
            $val['picture'] = $data['photoUrl'];
            $val['email_verified'] = 1;
            $val['username'] = $val['email'];

            
            switch ($provider) {
                case 'FACEBOOK':
                    # code...
                    $val['facebook_id'] = $data['id'];
                    $val['facebook_id_token'] = $data['authToken'];
                    break;
    
                case 'GOOGLE':
                    # code...
                    $val['google_id'] = $data['id'];
                    $val['google_id_token'] = $data['authToken'];
                    break;
                
                default:
                    # code...
                    break;
            }

            $user = User::create($val);
            $success['token'] =  $user->createToken('Occaplex')->accessToken; 

             $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         ]);

         $body = [
         'Messages' => [
         [
             'From' => [
                 'Email' => ENV('MAIL_FROM_ADDRESS'),
                 'Name' => ENV('MAIL_FROM_NAME')
             ],
             'To' => [
                 [
                     'Email' => $user->email,
                     'Name' => $user->name,
                 ]
             ],
            
             'TemplateID' => 2096500,
             'TemplateLanguage' => true,
            'Subject' => "",   
         ]
     ]
 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);
 }
            return response()->json([
                'status' => 'success',
                'token' => $success['token'],
                'message' => 'User registration successful',
                'data' => $user
            ], 200);
            
        }

        

    

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'phone' => 'required', 
            'country' => 'required',
            'city' => 'required', 
            'zipcode' => 'required', 
            'username' => ['required', 'string', 'max:255', 'unique:users', 'alpha_dash'], 
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);  
        }

        $data = $request->all();
        $data['uuid'] = Str::uuid();
        $data['password'] = bcrypt($data['password']);
        $data['email_verify_token'] =  md5(rand(10000, 99999));
        $verify['url'] = 'https://occaplex.com/verify/email/'. $data['username'].'/'.$data['email_verify_token'];
        $verify['name'] = $data['name'];
        $user = User::create($data);


        // $success['token'] =  $user->createToken('Occaplex')->accessToken; 
        
        $userData = User::where('email', $data['email'])->first();

        if ($user) {
            // Send confirmatation email

            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('emails.verify_register', $verify, function($message) use ($data)
            {
                $message
                    ->from('info@aitechma.com', 'Occaplex')
                    ->to($data['email'], $data['name'])
                    ->subject('Verify your email address');
            });
           return response()->json([
                'status' => 'success',
                // 'token' => $success['token'],
                'message' => 'User registration successful',
                'data' => $userData
           ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'User registration not successful'
            ], 401);
        }
        


    }  

    public function verifyEmail(Request $request) {
        $data = $request->all();

        if(User::where('username', $data['username'])->where('email_verify_token', $data['token'])->where('email_verified', 1)->count() > 0) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Email already verified'
           ], 200); 
        }
        $user = User::where('username',$data['username'])->first();

        if(User::where('username', $data['username'])->where('email_verify_token', $data['token'])->update(['email_verified' => 1])) {
        	 $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         ]);

         $body = [
         'Messages' => [
         [
             'From' => [
                 'Email' => ENV('MAIL_FROM_ADDRESS'),
                 'Name' => ENV('MAIL_FROM_NAME')
             ],
             'To' => [
                 [
                     'Email' => $user->email,
                     'Name' => $user->name,
                 ]
             ],
            
             'TemplateID' => 2096500,
             'TemplateLanguage' => true,
            'Subject' => "",  
         ]
     ]
 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);
 
            return response()->json([
                'status' => 'success',
                'message' => 'Email verification successful'
           ], 200); 
        }else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Email verification failed'
            ], 401); 
        }

    }

    public function resendVerifyEmail(Request $request) {

        $data = $request->all();
        $data['email_verify_token'] =  md5(rand(10000, 99999));
        $verify['url'] = 'https://occaplex.com/verify/email/'. $data['username'].'/'.$data['email_verify_token'];
        $verify['name'] = $data['username'];

        if(User::where('username', $data['username'])->update(['email_verify_token' => $data['email_verify_token'], 'email_verified' => 0])) {
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('emails.verify_register', $verify, function($message) use ($data)
            {
                $message
                    ->from('info@aitechma.com', 'Occaplex')
                    ->to($data['email'], $data['username'])
                    ->subject('Verify your email address');
            });
           return response()->json([
                'status' => 'success',
                // 'token' => $success['token'],
                'message' => 'Email verification sent successfully',
                'data' => $data
           ], 200);

        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Email verification resend failed'
            ], 401); 
        }
    }
    
    public function login(Request $request) {
        // $validator = Validator::make($request->all(), [ 
        //     'password' => 'required', 
        //     'email' => 'required|email'
        // ]);
        
        // if ($validator->fails()) { 
        //     return response()->json(['error'=>$validator->errors()], 401);  
        // }

        // $data = $request->all();

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('Occaplex')->accessToken; 

            if($user['email_verified'] == 0) {
                return response()->json([
                    'status' => 'unverified',
                    'message' => 'User email has not been verified',
                    'data' => ["username" => $user['username'], "email" => $user['email']]
                ], 200);

            } else {
                return response()->json([
                'status' => 'success',
                'token' => $success['token'],
                'message' => 'User login successful',
                'data' => $user->where('email', request('email'))->first()
                ], 200); 
            }
            
        } 
        else{ 
            return response()->json([
                'status' => 'failed',
                'message' => 'User unauthorized'
            ], 401); 
        }

    }

    public function MyProfile() {

        $user = Auth::user();
        $business_status = Vendor::where('user_id', $user['uuid'])->pluck('business_confirmed')->first();
        $user['account_type'] = $business_status == 0 ? 'customer' : 'vendor';
        $user['isVendor'] = Vendor::where('user_id', $user['uuid'])->exists();
        $user['vendor_status'] = Vendor::where('user_id', $user['uuid'])->pluck('business_confirmed')->first();
        $user['vendor_application_editable'] = Vendor::where('user_id', $user['uuid'])->pluck('edit_enabled')->first() == 0 ? false : true;
        if($user['isVendor']) {
            $user['vendor_details'] = Vendor::where('user_id', $user['uuid'])->first();
        }

        return response()->json([
            'status' => 'success',
            'data' => $user
       ], 200); 

    }

    public function UserProfile(Request $request) {
        $data = $request->all();
        $user = User::where('uuid', $data['uuid'])->first();
        $business_status = Vendor::where('user_id', $user['uuid'])->pluck('business_confirmed')->first();
        $user['account_type'] = $business_status == 0 ? 'customer' : 'vendor';
        return response()->json([
            'status' => 'success',
            'data' => $user
       ], 200); 
    }
    
    public function UpdateProfile(Request $request) {
        $data = $request->all();
        
        $user = Auth::user();

        unset($data['isVendor']);
        unset($data['account_type']);
        unset($data['vendor_status']);
        unset($data['vendor_application_editable']);
        unset($data['vendor_details']);
        
        if(User::where('uuid', $user['uuid'])->update($data)) {
            return response()->json([
            'status' => 'success',
            'message' => 'User profile updated successfully',
            'data' => User::where('uuid', $user['uuid'])->first()
         ], 200);   
        } else {
            return response()->json([
            'status' => 'failed',
            'message' => 'Error occurred while updating profile.'
         ], 401);
        }
        
        
    }
    
    public function UpdateProfilePicture(Request $request) {
        $data = $request->all();
        
        $user = Auth::user();
        
        $options = array(
            // "overlay"=>"retina_a4wkjj", 
            // "width"=>150, 
            // "opacity"=>70,
            // "gravity"=>"north_west", 
            // "x"=>25, 
            // "y"=>25
        );
        
        if(Cloudder::upload($data['image'], $options)) {
            $upload_result = Cloudder::getResult();
            $data = array(
              'picture' => $upload_result['url']
            );
            
           if(User::where('uuid', $user['uuid'])->update($data)) {
                return response()->json([
                'status' => 'success',
                'message' => 'User picture updated successfully',
                'data' => User::where('uuid', $user['uuid'])->first()
             ], 200);   
            } else {
                return response()->json([
                'status' => 'failed',
                'message' => 'Error occurred while updating profile picture.'
             ], 401);
            }
        
        };
        
    }
    
    public function userVendor($id) {
        
        $vendor = Vendor::where('user_id', $id)->first();
        
        return response()->json([
                'status' => 'success',
                'message' => 'User vendor retrieved',
                'data' => $vendor
             ], 200); 
        
    }
    
    public function sendFriendRequest($friend) {
        $user = Auth::user();
        
        $data = Array(
            'sender'=> $user['uuid'],
            'friend' => $friend,
            'accepted' => false,
            'blocked' => false
            );
            
        if($user['uuid'] == $friend) {
            return response()->json([
                'status'=> 'failed',
                'message'=> 'You can not send a request to yourself'
                ], 401);
        }
        
        if(Friend_connection::where('sender', $user['uuid'])->where('friend', $friend)->count() > 0) {
            
            return response()->json([
                'status' => 'failed',
                'message' => 'Invalid request'
             ], 401);
            
        }
        
        Friend_connection::where('sender', $user['uuid'])->where('friend', $friend)->create($data);
        
        return response()->json([
                'status' => 'success',
                'message' => 'Friend request sent successfully'
             ], 200); 
    }
    
    public function acceptFriendRequest($request) {
        $user = Auth::user();
        
        $data = Array(
            'accepted' => true,
            );
        
        if(Friend_connection::where('sender', $user['uuid'])->where('id', $request)->count() > 0) {
            
            return response()->json([
                'status' => 'failed',
                'message' => 'Invalid request'
             ], 401);
            
        }
        
        Friend_connection::where('friend', $user['uuid'])->where('id', $request)->update($data);
        
        return response()->json([
                'status' => 'success',
                'message' => 'Friend request accepted successfully'
             ], 200); 
    }
    
    public function rejectFriendRequest($request) {
        $user = Auth::user();
        
        // $data = Array(
        //     'accepted' => true,
        //     );
        
        if(Friend_connection::where('sender', $user['uuid'])->where('id', $request)->count() > 0) {
            
            return response()->json([
                'status' => 'failed',
                'message' => 'Invalid request'
             ], 401);
            
        }
        
        Friend_connection::where('friend', $user['uuid'])->where('id', $request)->delete();
        
        return response()->json([
                'status' => 'success',
                'message' => 'Friend request rejected successfully'
             ], 200); 
    }
    
    public function removeFriend($friend) {
        $user = Auth::user();
        
        // $data = Array(
        //     'accepted' => true,
        //     );
        
        // if(Friend_connection::where('sender', $user['uuid'])->where('id', $request)->count() > 0) {
            
        //     return response()->json([
        //         'status' => 'failed',
        //         'message' => 'Invalid request'
        //      ], 401);
            
        // }
        
        Friend_connection::where('friend', $user['uuid'])->where('sender', $friend)->delete();
        Friend_connection::where('sender', $user['uuid'])->where('friend', $friend)->delete();
        
        return response()->json([
                'status' => 'success',
                'message' => 'Friend removed successfully'
             ], 200); 
    }
    
    public function myRequests() {
        $user = Auth::user();
        
        $requests = Friend_connection::where('friend', $user['uuid'])->where('accepted', false)->get();
        $requests_count = Friend_connection::where('friend', $user['uuid'])->where('accepted', false)->count();
        
        $senders = [];
        foreach($requests as $sender) {
            $sendingR = User::where('uuid', $sender['sender'])->get();
            
            array_push($senders, $sendingR);
        }
        
        return response()->json([
                'status' => 'success',
                'message' => 'Friend requests retrieved',
                'data' => $senders,
                'count' => $requests_count
             ], 200); 
    }
    
    public function userFriends($id) {
        
        $friends = Friend_connection::where('friend', $id)->orWhere('sender', $id)->where('accepted', true)->get();
        $friends_count = Friend_connection::where('friend', $id)->orWhere('sender', $id)->where('accepted', true)->count();
        
        
        return response()->json([
                'status' => 'success',
                'message' => 'Friends list retrieved',
                'data' => $friends,
                'count' => $friends_count
             ], 200);  
    }
    
    // public function generatePasswordResetLink(Request $request) {
    //     $data = $request->all();
    //     $identity = $data['id'];
        
    //     $email = User::where('email', $identity)->orWhere('username', $identity)->pluck('email')->first();
    //     $name = User::where('email', $identity)->orWhere('username', $identity)->pluck('name')->first();
    //     $num = rand(10000000, 999999999);
    //     $token = bcrypt($num);
        
    //     $to_email = 'ibraheemadepoju@gmail.com';
    //     $subject = 'Occaplex password reset';
    //     $message = 'Dear '.$name.',<br>Click the link below to complete your password reset. <br><br> https://occaplex.com/?token='.$token.' <br><br><br>Best regards, <br><b>Occaplex</b>';
    //     $headers = 'From: info@aiprinta.com';
    //     mail($to_email,$subject,$message,$headers);
    //         return response()->json([
    //             'status' => 'success',
    //             'message' => 'Password reset link sent'
    //          ], 200);
    // }

    public function changePassword(Request $request) {
        $data = $request->all();
        
        $user = Auth::user();
        
        if(!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            //return $this->sendError('Current password is invalid', 'Password not updated');
            return response()->json([
                'status' => 'failed',
                'message' => 'Current password is invalid,Password not updated'
            ]);
        }
        
        User::where('uuid', $user['uuid'])->update(['password'=> bcrypt($data['new_password'])]);
        
        //return $this->sendResponse(User::where('uuid', $user['uuid'])->first(), 'User password updated successfully');

        return response()->json([
            'status' => 'success',
            'message' => 'Password change successful'
        ],200);
        
    }
    
    public function passwordReset(Request $request) {
        $data = $request->all();
        
        if(User::where('email', $data['email'])->count() > 0) {
            $dataEmail = [
                    'name' => User::where('email', $data['email'])->pluck('name')->first(),
                    'pin' => mt_rand(10000000,99999999)
                    
                ];
                
            User::where('email', $data['email'])->update(['remember_token' => $dataEmail['pin']]);
            
            
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('emails.password_reset', $dataEmail, function($message) use ( $data)
            {
                $message->from('info@aitechma.com', 'Occaplex');
            
                // $message->to($data['email'])->cc('bar@example.com');
                $message->to($data['email']);
                $message->subject('Occaplex password reset');
            
                // $message->attach($pathToFile);
            });
            return response()->json([
                'status' => 'success',
                'message' => 'Password reset pin sent to your email'
             ], 200); 
            
            //  return $this->sendResponse('Password reset pin sent to your email', 'User password pin generated successfully');
            
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'User account does not exists'
             ], 200); 
            // return $this->sendError('User account does not exists', 'Password reset failed');
        }
    }
    
    public function passwordResetValidate(Request $request) {
        $data = $request->all();
        
        if(User::where('email', $data['email'])->where('remember_token', $data['pin'])->update(["password" => bcrypt($data['new_password'])])) {
            
            User::where('email', $data['email'])->where('remember_token', $data['pin'])->update(["remember_token" => null]);

            return response()->json([
                'status' => 'success',
                'message' => 'User password reset successfully'
             ], 200); 
            
            // return $this->sendResponse('Password reset', 'User password reset successfully');
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Password reset failed'
             ], 200); 
            // return $this->sendError('Reset failed', 'Password reset failed');
        }
    }

    public function updateAccountType(Request $request)
    {
        $data = $request->all();
        $user = User::where('uuid',$data['uuid'])->first();
        //$user = Auth::user();
        if(Vendor::where('user_id',$user['uuid'])->exists())
        {
            $user['account_type'] = 'vendor';
            $user->save();

            return response()->json([
                'message' => 'Account type updated'
            ],202);
        }else
        {
            return response()->json([
                'message' => 'This user is not a vendor'
            ],200);
        }

    }
     public function mailTest(Request $request)
    {
//         $user = Auth::user();

//         $mj = new \Mailjet\Client(ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET'),true,['version' => 'v3.1']);
//         $body = [
//     'Messages' => [
//         [
//             'From' => [
//                 'Email' => ENV('MAIL_FROM_ADDRESS'),
//                 'Name' => ENV('MAIL_FROM_NAME')
//             ],
//             'To' => [
//                 [
//                     'Email' => "$user->email",
//                     'Name' => $user->username
//                 ]
//             ],
//             'Subject' => $request->subject,
//             'TextPart' => $request->body,
//             // 'HTMLPart' => "<h3>Dear passenger 1, welcome to <a href=\"https://www.mailjet.com/\">Mailjet</a>!</h3><br />May the delivery force be with you!"
//         ]
//     ]
// ];
//      $response = $mj->post(Resources::$Email, ['body' => $body]);
//      return response()->json()
//     // $response->success() && var_dump($response->getData());

        $body = [
    'Messages' => [
        [
        'From' => [
            'Email' => ENV('MAIL_FROM_ADDRESS'),
            'Name' => ENV('MAIL_FROM_NAME')
        ],
        'To' => [
            [
            'Email' => "olanaseyezid@gmail.com",
            'Name' => "Olanase"
            ]
        ],
        'Subject' => $request->subject,
        'TextPart' => $request->body,
        'HTMLPart' => "<h3>Dear User, welcome to Mailjet!</h3><br />May the delivery force be with you!"
        ]
    ]
];
 
$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'https://api.mailjet.com/v3.1/',
]);
 
$response = $client->request('POST', 'send', [
    'json' => $body,
    'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')]
]);
 
if($response->getStatusCode() == 200) {
    $body = $response->getBody();
    $response = json_decode($body);
    if ($response->Messages[0]->Status == 'success') {
        echo "Email sent successfully.";
    }
}
     }

    public function generateTemplate(Request $request)
    {
        $body = $request->all();
        $client = new Client([
            'base_uri' => 'https://api.mailjet.com/v3/REST/',
        ]);
        $response = $client->request('POST','template', [
            'json' => $body,
            'Content-Type' => 'application/json',
            'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')]
        ]);

        if($response->getStatusCode() == 200)
        {
            $body = $response->getBody();
            $response = json_decode($body);

            return response()->json([
                'message' => 'Template created'
            ],200);
            // if ($response->Messages[0]->Status == 'success')
            // {
            //    var_dump($response->getData());
            // }
        }

    }

    public function getTemplates()
    {
        $client = new Client([
            'base_uri' => 'https://api.mailjet.com/v3/REST/',
        ]);

        $response = $client->request('GET','template', [
            'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')]
        ]);

        if($response->getStatusCode() == 200)
        {
             $body = $response->getBody();
             $response = json_decode($body);

             return response()->json([
                'data' => $response
             ],200);
            // //if ($response->Messages[0]->Status == 'success')
            // //{
            //    $response->getData();
            // //}
            //$response->success() && var_dump($response->getData());
        }

    }

    // public function verifyEmailTemplate(Request $request,$templateId)
    // {
    //     $client = new Client([
    //         'base_uri' => 'https://api.mailjet.com/v3/REST/',
    //     ]);
    //     //$body = $request->all();
    //     // $template_ID = $templateId;
    //     $body = [
    //       'Headers' => "",
    //       'Html-part' => "<h3>Dear {{ user }}, </br> Kindly use the link to verify your email. </br> {{ link }} </h3>",
    //       'MJMLContent' => "",
    //       'Text-part' => "Dear passenger, welcome to Mailjet! May the delivery force be with you!"
    //     ];
    //     $response = $client->request('POST','template/2044899/detailcontent', [
    //         'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
    //         'json' => $body,
    //     ]);

    //     if($response->getStatusCode() == 200)
    //     {
    //          $body = $response->getBody();
    //          $response = json_decode($body);

    //          return response()->json([
    //             'data' => $response
    //          ],200);
    //     }

    // }

//     public function sendMail(Request $request)
//     {
//         $client = new Client([
//             'base_uri' => 'https://api.mailjet.com/v3.1/',
//         ]);

//         $body = [
//     'Messages' => [
//         [
//             'From' => [
//                 'Email' => ENV('MAIL_FROM_ADDRESS'),
//                 'Name' => ENV('MAIL_FROM_NAME')
//             ],
//             'To' => [
//                 [
//                     'Email' => "olanasetoyin@gmail.com",
//                     'Name' => "Yezid"
//                 ]
//             ],
//             'TemplateID' => 2044899,
//             'TemplateLanguage' => true,
//             'Subject' => "Verify your Email!"
//         ]
//     ]
// ];
//         $response = $client->request('POST','send', [
//             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
//             'json' => $body,
//         ]);

//         if($response->getStatusCode() == 200) {
//     $body = $response->getBody();
//     $response = json_decode($body);
//     if ($response->Messages[0]->Status == 'success') {
//         echo "Email sent successfully.";
//     }
// }
     
//     }
}


