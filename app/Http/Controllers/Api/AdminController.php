<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Vendor;
use App\Listing;
use App\Category;
use App\Event_Type;
use App\Order;
use Illuminate\Support\Str;
use App\Friend_connection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use JD\Cloudder\Facades\Cloudder;
use DB;
use GuzzleHttp\Client;

class AdminController extends Controller
{
    //
    public function getUsers() {
        $admin = Auth::user();

        if($admin['role_id'] == 1) {
            $users = User::get();

            return response()->json([
                'status' => 'success',
                'data' => $users
            ],200);
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function getListingsByVendor($vendorUuid)
    {

        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Vendor::where('uuid',$vendorUuid)->exists())
            {
                $listings = Listing::where('vendor',$vendorUuid)->get();

                return response()->json([
                    'message' => 'Listings retrieved by vendor',
                    'data' => $listings
                ],202);
            }else
            {
                return response()->json([
                    'message' => 'Vendor not found'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized User'
            ],401);
        }
    }

    public function getUser($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            //$user = User::find($uuid);
            if(User::where('uuid',$uuid)->exists())
            {
                $user = User::where('uuid',$uuid)->get();
                return response()->json([
                    'data' => $user
                ],200);
            } else
            {
                return response()->json([
                    'message' => 'User does not exists'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function getVendors()
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            $vendors = Vendor::get();
            if(count($vendors) > 0){
                return response()->json([
                'status'=>'Success',
                'data'=>$vendors
            ],200);
            }else
            {
                return response()->json([
                    'message' => 'No vendors on ground'
                ],404);
            }
            
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }

    }

    public function getVendor($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            $vendor = Vendor::find($uuid);
            if(Vendor::where('uuid',$uuid)->exists())
            {
                $vendor = $vendor::where('uuid',$uuid)->first();
                $vendor['service_category'] = Category::where('uuid',$vendor['service_category'])->get();
                return response()->json([
                    'data'=> $vendor,
                    //'service category' => $service_category
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'Vendor does not exist'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }

    }

    public function approveVendorBusiness(Request $request,$uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            //$vendor = Vendor::find($uuid);
            if(Vendor::where('uuid',$uuid)->exists())
            {
                $vendor = Vendor::where('uuid',$uuid)->first();
                $vendor->business_confirmed = 1;
                $vendor->save();

                $message = is_null($request->message) ? "" : $request->message;

            $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

		         $body = [
		         'Messages' => [
		         [
		             'From' => [
		                 'Email' => ENV('MAIL_FROM_ADDRESS'),
		                 'Name' => ENV('MAIL_FROM_NAME')
		             ],
		             'To' => [
		                 [
		                     'Email' => $vendor->business_email,
		                     'Name' => $vendor->username,
		                 ]
		             ],
		            
		             'TemplateID' => 2106434,
		             'TemplateLanguage' => true,
		            'Subject' => "Vendor Application Approved",
		             'Variables' => [
		                 "username" => $vendor->username,
		                 "business_name" => $vendor->business_name,
		                 "reason" => $message
		             ]    
		         ]
		     ]
		 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);

         $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

         $body = [
		         'Messages' => [
		         [
		             'From' => [
		                 'Email' => ENV('MAIL_FROM_ADDRESS'),
		                 'Name' => ENV('MAIL_FROM_NAME')
		             ],
		             'To' => [
		                 [
		                     'Email' => $vendor->business_email,
		                     'Name' => $vendor->username,
		                 ]
		             ],
		            
		             'TemplateID' => 2107357,
		             'TemplateLanguage' => true,
		            'Subject' => "Welcome Vendor",
		             'Variables' => [
		                 "business_name" => $vendor->business_name
		             ]    
		         ]
		     ]
		 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);


        return response()->json([
                'message' => 'Vendor Business Approved',
                'data' => $vendor
            ],202);
        }else
        {
            return response()->json([
                'message'=> 'Vendor does not exist'
            ],404);
        }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user',
            ],401);
        }
    }

    public function denyVendorBusiness(Request $request,$uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Vendor::where('uuid',$uuid)->exists())
            {
                $vendor = Vendor::where('uuid',$uuid)->first();
                $vendor->business_confirmed = 2;
                $vendor->admin_denial_message = is_null($request->message) ? null :$request->message;
                $vendor->save();

                 $message = is_null($request->message) ? "" : $request->message;

                $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

	         $body = [
	         'Messages' => [
	         [
	             'From' => [
	                 'Email' => ENV('MAIL_FROM_ADDRESS'),
	                 'Name' => ENV('MAIL_FROM_NAME')
	             ],
	             'To' => [
	                 [
	                     'Email' => $vendor->business_email,
	                     'Name' => $vendor->username,
	                 ]
	             ],
	            
	             'TemplateID' => 2106476,
	             'TemplateLanguage' => true,
	            'Subject' => "Vendor Application Denied",
	             'Variables' => [
	                 "username" => $vendor->username,
	                 "business_name" => $vendor->business_name,
	                 "reason" => $message
	             ]    
	         ]
	     ]
	 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);

         
                return response()->json([
                    'message' => 'Vendor Denied',
                    'data' => $vendor
                ],202);
            }else
            {
                return response()->json([
                    'message' => 'Vendor does not exist',
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user',
            ],401);
        }
    }

    public function suspendVendorBusiness(Request $request,$uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Vendor::where('uuid',$uuid)->exists())
            {
                $vendor = Vendor::where('uuid',$uuid)->first();
                $vendor->business_confirmed = 3;
                $vendor->admin_suspension_message = is_null($request->message) ? null : $request->message;
                $vendor->save();

                $message = is_null($request->message) ? "" : $request->message;

                $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

		         $body = [
		         'Messages' => [
		         [
		             'From' => [
		                 'Email' => ENV('MAIL_FROM_ADDRESS'),
		                 'Name' => ENV('MAIL_FROM_NAME')
		             ],
		             'To' => [
		                 [
		                     'Email' => $vendor->business_email,
		                     'Name' => $vendor->username,
		                 ]
		             ],
		            
		             'TemplateID' => 2106715,
		             'TemplateLanguage' => true,
		            'Subject' => "",
		             'Variables' => [
		                 "username" => $vendor->username,
		                 "business_name" => $vendor->business_name,
		                 'reason' => $message,
		             ]    
		         ]
		     ]
		 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);
        return response()->json([
            'message' => 'Vendor Suspended',
            'data' => $vendor
        ],202);
        }else
        {
            return response()->json([
                'message' => 'Vendor does not exist',
            ],404);
        }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user',
            ],401);
        }
    }

    public function unapproveVendorBusiness(Request $request,$uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Vendor::where('uuid',$uuid)->exists())
            {
                $vendor = Vendor::where('uuid',$uuid)->first();
                $vendor->business_confirmed = 4;
                $vendor->admin_unapproved_message = is_null($request->message) ? null : $request->message;
                $vendor->save();

                $message = is_null($request->message) ? "" : $request->message;

                $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

		         $body = [
		         'Messages' => [
		         [
		             'From' => [
		                 'Email' => ENV('MAIL_FROM_ADDRESS'),
		                 'Name' => ENV('MAIL_FROM_NAME')
		             ],
		             'To' => [
		                 [
		                     'Email' => $vendor->business_email,
		                     'Name' => $vendor->username,
		                 ]
		             ],
		            
		             'TemplateID' => 2106736,
		             'TemplateLanguage' => true,
		            'Subject' => "",
		             'Variables' => [
		                 "username" => $vendor->username,
		                 'reason' => $message
		             ]    
		         ]
		     ]
		 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);


        return response()->json([
            'message' => 'Vendor Unapproved',
            'data' => $vendor
        ],202);
	    }else
	    {
	        return response()->json([
	            'message' => 'Vendor does not exist',
	        ],404);
	    }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user',
            ],401);
        }
    }

    public function getListings()
    {
        $admin = Auth::user();
        if($admin['role_id'] == 1 )
        {
            $listings = Listing::get();
            if(count($listings) > 0)
            {
                 return response([
                'data' => $listings,
                'message' => 'Listings retrieved successfully'
                ],200);
            }else
            {
                return response()->json([
                    'message'=>'No listings on ground'
                ],404);
            }
           
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }
    public function getListing($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Listing::where('uuid',$uuid)->exists())
            {
                $listing = Listing::where('uuid',$uuid)->get()->toJson();

                return response([
                    'data' => $listing,
                    'message' => 'Listing retrieved'
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'This listing does not exist'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function updateListingStatusToPending($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Listing::where('uuid',$uuid)->exists())
            {
                $listing = Listing::where('uuid',$uuid)->first();
                $listing->status = 0;
                $listing->save();

                return response()->json([
                    'message' => 'Listing updated to Pending',
                    'data' => $listing
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'This listing doesnt exist'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function updateListingStatusToApprove($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Listing::where('uuid',$uuid)->exists())
            {
                $listing = Listing::where('uuid',$uuid)->first();
                $listing->status = 1;
                $listing->save();
                $vendor = Vendor::where('uuid',$listing->vendor)->first();

                $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

		         $body = [
		         'Messages' => [
		         [
		             'From' => [
		                 'Email' => ENV('MAIL_FROM_ADDRESS'),
		                 'Name' => ENV('MAIL_FROM_NAME')
		             ],
		             'To' => [
		                 [
		                     'Email' => $vendor->business_email,
		                     'Name' => $vendor->username,
		                 ]
		             ],
		            
		             'TemplateID' => 2106753,
		             'TemplateLanguage' => true,
		            'Subject' => "",
		             'Variables' => [
		                 "username" => $vendor->username,
		                 "listing" => $listing->title
		             ]    
		         ]
		     ]
		 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);


            return response()->json([
                'message' => 'Listing Approved',
                'data' => $listing
            ],200);
            }else
            {
                return response()->json([
                    'message' => 'This listing doesnt exist'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function updateListingStatusToUnapprove($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Listing::where('uuid',$uuid)->exists())
            {
                $listing = Listing::where('uuid',$uuid)->first();
                $listing->status = 2;
                $listing->save();

                return response()->json([
                    'message' => 'Listing Unapproved',
                    'data' => $listing
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'This listing doesnt exist'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function updateListingStatusToSuspend($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Listing::where('uuid',$uuid)->exists())
            {
                $listing = Listing::where('uuid',$uuid)->first();
                $listing->status = 3;
                $listing->save();

                return response()->json([
                    'message' => 'Listing Suspended',
                    'data' => $listing
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'This listing doesnt exist'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function getCategories()
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            $categories = Category::get();
            if(count($categories) > 0)
            {
                return response()->json([
                    'Message' => "Categories Regrieved",
                    'data' => $categories
                ],200);
            }else
            {
                return response()->json([
                    'message'=> 'No categories Availaible'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function getCategory($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Category::where('uuid',$uuid)->exists())
            {
                $category = Category::where('uuid',$uuid)->get();

                return response()->json([
                    'message' => 'Category retrieved',
                    'data' => $category
                ],202);
            }
            else
            {
                return response()->json([
                    'message' => 'This category does not exists'
                ],404);
            }
        }else
        {
            return response()->json([
                'message'=> 'Unauthorized user'
            ],401);
        }
    }

    public function editCategory(Request $request,$uuid)
    {
        $admin = Auth::user();
        $data = $request->all();

        if($admin['role_id'] == 1)
        {
            if(Category::where('uuid',$uuid)->exists())
            {
                $category = Category::where('uuid',$uuid)->first();
                $category->update($data);

                return response()->json([
                    'message' => 'Category Updated',
                    'data' => $category
                ],202);
            }else
            {
                return response()->json([
                    'message' => 'This category cant be found'
                ],404);
            }
        }else
        {
            return response()->json([
                'message'=> 'Unauthorized User'
            ],401);
        }
    }

    public function deleteCategory($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Category::where('uuid',$uuid)->exists())
            {
                $category = Category::where('uuid',$uuid)->first();
                $category->delete();

                return response()->json([
                    'message' => 'Category deleted'
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'This category doesnt exist'
                ],404);
            }   
        }else
        {
            return response()->json([
                'message' => 'Unauthorized User'
            ],401);
        }
        
    }
    public function getEventTypes()
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            $eventType = Event_Type::get();
            if(count($eventType) > 0)
            {
               return response()->json([
                'message' => 'Event types retrieved',
                'data' => $eventType
                ],200); 
            }else
            {
                return response()->json([
                    'message' => 'No event type availaible'
                ],404);
            }
            
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }   

    public function getEventType($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1){
            if(Event_Type::where('uuid',$uuid)->exists())
            {
                $eventType = Event_Type::where('uuid',$uuid)->get();

                return response()->json([
                    'message' => 'Event type retrieved',
                    'data' => $eventType
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'This event type doesnt exist'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }
    public function deleteEventType($uuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Event_Type::where('uuid',$uuid)->exists())
            {
                $eventType = Event_Type::where('uuid',$uuid)->first();
                $eventType->delete();

                return response()->json([
                    'message' => 'Event type Deleted'
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'This event type cant be found'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function editEventType(Request $request,$uuid)
    {
        $admin = Auth::user();
        $data = $request->all();

        if($admin['role_id'] == 1)
        {
            if(Event_Type::where('uuid',$uuid)->exists())
            {
                $eventType = Event_Type::where('uuid',$uuid)->first();
                $eventType->update($data);

                return response()->json([
                    'message' => 'Event Type updated',
                    'data' => $eventType
                ],200);
            }else
            {
                return response()->json([
                    'message' => 'This event type cant be found'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized user'
            ],401);
        }
    }

    public function getOrdersByListing($listingUuid)
    {
        $admin = Auth::user();

        if($admin['role_id'] == 1)
        {
            if(Listing::where('uuid',$listingUuid)->exists())
            {
                $orders = Order::where('listing_type',$listingUuid)->get();

                return response()->json([
                    'message' => 'Orders retrieved by Listing',
                    'data' => $orders
                ],202);
            }else
            {
                return response()->json([
                    'message' => 'Listing not found'
                ],404);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized User'
            ],401);
        }
    }

    public function adminAnalytics(Request $request)
    {
        $admin = Auth::user();
        $date = $request->all();

        if($admin['role_id'] == 1)
        {
            if($request->has('from_date'))
            {
                $total_users = User::whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                $total_vendors_approved = Vendor::where('business_confirmed',1)->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                $total_vendors_application = Vendor::where('business_confirmed',0)->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                $total_listings = Listing::whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                $total_orders = Listing::whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();;
                $total_sales = DB::table('orders')->whereBetween('created_at',[$date['from_date'],$date['to_date']])->sum('sales_price');
                $total_checkout_fees = DB::table('orders')->whereBetween('created_at',[$date['from_date'],$date['to_date']])->sum('checkout_fee');

                return response()->json([
                    'message' => 'Admin Analytics betwwen '.$date['from_date'] . ' and '. $date['to_date'],
                    'Total users' => $total_users,
                    'Approved Vendors' => $total_vendors_approved,
                    'Vendor Applications' => $total_vendors_application,
                    'Total Listings' => $total_listings,
                    'Total Orders' => $total_orders,
                    'Total sales' => $total_sales,
                    'Total Checkout Fee' => $total_checkout_fees 
                ]);
            }else
            {
                $total_users = count(User::all());
                $total_vendors_approved = Vendor::where('business_confirmed',1)->count();
                $total_vendors_application = Vendor::where('business_confirmed',0)->count();
                $total_listings = count(Listing::all());
                $total_orders = count(Order::all());
                $total_sales = DB::table('orders')->sum('sales_price');
                $total_checkout_fees = DB::table('orders')->sum('checkout_fee');

                return response()->json([
                    'Total users' => $total_users,
                    'Approved Vendors' => $total_vendors_approved,
                    'Vendor Applications' => $total_vendors_application,
                    'Total Listings' => $total_listings,
                    'Total Orders' => $total_orders,
                    'Total sales' => $total_sales,
                    'Total Checkout Fee' => $total_checkout_fees 
                ]);
            }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized User'
            ],401);
        }
    }

    public function vendorAnalytics(Request $request,$vendorUuid)
    {
        $admin = Auth::user();

        $date = $request->all();
        if($admin['role_id'] == 1)
        {
            if($request->has('from_date'))
            {
                if(Vendor::where('uuid',$vendorUuid)->exists())
                {
                    $total_listings = Listing::where('vendor',$vendorUuid)->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();

                    $total_orders = Order::where('vendor_uuid',$vendorUuid)->count();
                    $new_orders = Order::where([
                        ['vendor_uuid',$vendorUuid],
                        ['order_status','submitted']
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                    $active_orders = Order::where([
                        ['vendor_uuid',$vendorUuid],
                        ['order_status','processing']
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                    $completed_orders = Order::where([
                        ['vendor_uuid',$vendorUuid],
                        ['order_status','completed']
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();
                    $approved_listings = Listing::where([
                        ['vendor',$vendorUuid],
                        ['status',1]
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->count();

                    $total_revenue = Order::where([
                        ['vendor_uuid',$vendorUuid],
                        ['order_status','completed']
                    ])->whereBetween('created_at',[$date['from_date'],$date['to_date']])->sum('vendor_revenue');

                    $pending_revenue = Order::where([
                        ['vendor_uuid',$vendorUuid],
                        ['order_status','submitted'],
                        //['order_status','processing'],
                    ])->orWhere('order_status','processing')->whereBetween('created_at',[$date['from_date'],$date['to_date']])->sum('vendor_revenue');

                    return response()->json([
                    'message' => 'Vendor Analytics betwwen '.$date['from_date'] . ' and '. $date['to_date'], 
                    'Listings' => $total_listings,
                    'Orders' => $total_orders,
                    'New Orders' => $new_orders,
                    'Active Orders' => $active_orders,
                    'Completed Orders' => $completed_orders,
                    'Approved Listings' => $approved_listings,
                    'Total Revenue' => $total_revenue,
                    'Pending Revenue' => $pending_revenue
                    ],200);
                }
            }

        if(Vendor::where('uuid',$vendorUuid)->exists())
        {
            $total_listings = Listing::where('vendor',$vendorUuid)->count();
            $total_orders = Order::where('vendor_uuid',$vendorUuid)->count();
            $new_orders = Order::where([
                ['vendor_uuid',$vendorUuid],
                ['order_status','submitted']
            ])->count();
            $active_orders = Order::where([
                ['vendor_uuid',$vendorUuid],
                ['order_status','processing']
            ])->count();
            $completed_orders = Order::where([
                ['vendor_uuid',$vendorUuid],
                ['order_status','completed']
            ])->count();
            $approved_listings = Listing::where([
                ['vendor',$vendorUuid],
                ['status',1]
            ])->count();

            $total_revenue = Order::where([
                ['vendor_uuid',$vendorUuid],
                ['order_status','completed']
            ])->sum('vendor_revenue');

            $pending_revenue = Order::where([
                ['vendor_uuid',$vendorUuid],
                ['order_status','submitted'],
                //['order_status','processing'],
            ])->orWhere('order_status','processing')->sum('vendor_revenue');

            return response()->json([
                'Listings' => $total_listings,
                'Orders' => $total_orders,
                'New Orders' => $new_orders,
                'Active Orders' => $active_orders,
                'Completed Orders' => $completed_orders,
                'Approved Listings' => $approved_listings,
                'Total Revenue' => $total_revenue,
                'Pending Revenue' => $pending_revenue
            ],200);
        }
        }else
        {
            return response()->json([
                'message' => 'Unauthorized User'
            ],401);
        }
        
    }
}
