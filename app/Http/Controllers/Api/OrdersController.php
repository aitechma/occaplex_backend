<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str; 
use App\User;
use App\Vendor;
use App\Order;
use App\Note;
use App\Listing;
use App\Calendar;
use Validator;
use GuzzleHttp\Client;

class OrdersController extends Controller
{
    //

    public function placeOrder(Request $request, $gateway = null) {
        $data = $request->all();
        $order['gref'] = 'G-OOC-OD'.hexdec(uniqid());
        $user = Auth::user();
        switch ($gateway) {
            case 'stripe':
                # code...
                $stripe_key = "sk_test_S9xvjDwWGfv51VQimfYJ1R9300t1XwUV6s";
                \Stripe\Stripe::setApiKey($stripe_key);
                $charge = \Stripe\Charge::create([
                    'amount' => $data['total_price'] * 100,
                    'currency' => 'cad',
                    'description' => 'Payment for order '.$order['gref'],
                    'source' => $data['payment_token'],
                    // 'customer' => Card::where('user_uuid', $customer)->pluck('stripe_customer_id')->first(),
                    'receipt_email' => User::where('uuid', $user['uuid'])->pluck('email')->first()
                ]);

                if($charge->status == 'succeeded' && $charge->paid == true) {

                    for ($i=0; $i < count($data['products']); $i++) { 
                        # code...
                        $order['uuid'] = Str::uuid();
                        $product = $data['products'][$i];
                        $order['ref'] = $order['gref'].'-'.$i;
                        $order['quantity'] = $product['quantity'];
                        $order['image'] = $product['image'];
                        $order['title'] = $product['name'];
                        $order['listing_type'] = $product['data']['listing_type'];
                        $order['sales_price'] = $product['price'];
                        $order['checkout_fee'] = 0.05 * (float) $order['sales_price'];
                        $order['shipping_fee'] = 0;
                        $order['tax'] = 0.00 * (float) $order['sales_price'];
                        $order['vendor_revenue'] = (float) $order['sales_price'] - (0.05 * $order['sales_price']);
                        $order['vendor_charge'] = 0.05 * $order['sales_price'];
                        $order['order_status'] = 'submitted';
                        $order['payment_status'] = 'success';
                        $order['payment_method'] = $data['payment_method'];
                        $order['product_uuid'] = $product['id'];
                        $order['total_price'] = $order['sales_price'] + $order['checkout_fee'] + $order['shipping_fee'] + $order['tax'];
                        $order['order_meta'] = $data['order_data'];
                        $order['payment_data'] = $charge;
                        $order['payment_ref_id'] = $charge->id;
                        $order['user_uuid'] = $user['uuid'];
                        $order['vendor_uuid'] = $product['data']['vendor'];
                        $order['date_needed'] = $data['date_needed'];
                        $order['order_note'] = $data['order_note'];
            
                        Order::create($order);
            
                        /**
                         * Send an email to vendor
                         */
            
                        $vendor_data_uuid = Vendor::where('uuid', $order['vendor_uuid'])->pluck('user_id')->first();
                        $vendor_user = User::where('uuid', $vendor_data_uuid)->first();

                        $client = new Client([
                         'base_uri' => 'https://api.mailjet.com/v3.1/',
                        ]);

                         $body = [
                         'Messages' => [
                         [
                             'From' => [
                                 'Email' => ENV('MAIL_FROM_ADDRESS'),
                                 'Name' => ENV('MAIL_FROM_NAME')
                             ],
                             'To' => [
                                 [
                                     'Email' => $vendor_user->email,
                                     'Name' => $vendor_user->name,
                                 ]
                             ],
                            
                             'TemplateID' => 2111910,
                             'TemplateLanguage' => true,
                            'Subject' => "New Listing Order Received",
                             'Variables' => [
                                "order_id" => $order->uuid,
                                "ref" => $order->listing_type,
                                "sales_price" => $order->sales_price
                            ]
                         ]
                        ]
                     ];
                     $response = $client->request('POST','send', [
                         'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
                         'json' => $body,
                     ]);
                    }
                }
                 $client = new Client([
                 'base_uri' => 'https://api.mailjet.com/v3.1/',
                ]);

             $body = [
             'Messages' => [
             [
                 'From' => [
                     'Email' => ENV('MAIL_FROM_ADDRESS'),
                     'Name' => ENV('MAIL_FROM_NAME')
                 ],
                 'To' => [
                     [
                         'Email' => $user->email,
                         'Name' => $user->name,
                     ]
                 ],
                
                 'TemplateID' => 2107663,
                 'TemplateLanguage' => true,
                'Subject' => "",
                 'Variables' => [
                    "username" => $user->name,
                    "gref" => $order['gref'],
                    "payment_ref_id" => $data['payment_ref_id'],
                    "sales_price" => $data['total_price']   
                         ]    
                     ]
                 ]
             ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);
                break;
            
            default:
                # code...
                for ($i=0; $i < count($data['products']); $i++) { 
                    # code...
                    $order['uuid'] = Str::uuid();
                    $product = $data['products'][$i];
                    $order['ref'] = $order['gref'].'-'.$i;
                    $order['quantity'] = $product['quantity'];
                    $order['image'] = $product['image'];
                    $order['title'] = $product['name'];
                    $order['listing_type'] = $product['data']['listing_type'];
                    $order['sales_price'] = $product['price'];
                    $order['checkout_fee'] = 0.05 * (float) $order['sales_price'];
                    $order['shipping_fee'] = 0;
                    $order['tax'] = 0.00 * (float) $order['sales_price'];
                    $order['vendor_revenue'] = (float) $order['sales_price'] - (0.05 * $order['sales_price']);
                    $order['vendor_charge'] = 0.05 * $order['sales_price'];
                    $order['order_status'] = 'submitted';
                    $order['payment_status'] = $data['payment_status'];
                    $order['payment_method'] = $data['payment_method'];
                    $order['product_uuid'] = $product['id'];
                    $order['total_price'] = $order['sales_price'] + $order['checkout_fee'] + $order['shipping_fee'] + $order['tax'];
                    $order['order_meta'] = $data['order_data'];
                    $order['payment_data'] = $data['payment_data'];
                    $order['payment_ref_id'] = $data['payment_ref_id'];
                    $order['user_uuid'] = $user['uuid'];
                    $order['vendor_uuid'] = $product['data']['vendor'];
                    $order['date_needed'] = $data['date_needed'];
                    $order['order_note'] = $data['order_note'];
        
                    Order::create($order);
        
                    /**
                     * Send an email to vendor
                     */
        
                    $vendor_data_uuid = Vendor::where('uuid', $order['vendor_uuid'])->pluck('user_id')->first();
                    $vendor_user = User::where('uuid', $vendor_data_uuid)->first();
        			
        			$client = new Client([
                         'base_uri' => 'https://api.mailjet.com/v3.1/',
                        ]);

                         $body = [
                         'Messages' => [
                         [
                             'From' => [
                                 'Email' => ENV('MAIL_FROM_ADDRESS'),
                                 'Name' => ENV('MAIL_FROM_NAME')
                             ],
                             'To' => [
                                 [
                                     'Email' => $vendor_user->email,
                                     'Name' => $vendor_user->name,
                                 ]
                             ],
                            
                             'TemplateID' => 2111910,
                             'TemplateLanguage' => true,
                            'Subject' => "New Listing Order Received",
                             'Variables' => [
                                "order_id" => $order->uuid,
                                "ref" => $order->listing_type,
                                "sales_price" => $order->sales_price
                            ]
                         ]
                        ]
                     ];
                     $response = $client->request('POST','send', [
                         'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
                         'json' => $body,
                     ]);
		        
            }
            $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

	         $body = [
	         'Messages' => [
	         [
	             'From' => [
	                 'Email' => ENV('MAIL_FROM_ADDRESS'),
	                 'Name' => ENV('MAIL_FROM_NAME')
	             ],
	             'To' => [
	                 [
	                     'Email' => $user->email,
	                     'Name' => $user->name,
	                 ]
	             ],
	            
	             'TemplateID' => 2107663,
	             'TemplateLanguage' => true,
	            'Subject' => "",
	             'Variables' => [
	                "username" => $user->name,
	                "gref" => $order['gref'],
	                "payment_ref_id" => $data['payment_ref_id'],
	                "sales_price" => $data['total_price']	
	             ]    
	         ]
	     ]
	 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);

                break;
        }
        

        return response()->json([
            'status' => 'success',
            'message' => 'Order created successfully',
            'order_ref' => $order['gref'] 
        ]);


        // \Stripe\Stripe::setApiKey('sk_test_S9xvjDwWGfv51VQimfYJ1R9300t1XwUV6s');

        // $data = $request->all();

        // $token = $data['token'];

        // $charge = \Stripe\Charge::create([
        //     'amount' => $data['total'],
        //     'currency' => 'cad',
        //     'description' => 'Example charge',
        //     'source' => $token,
        // ]);
    }


    public function getOrders($account) {
        $user = Auth::user();
        switch ($account) {
            case 'vendor':
                # code...
                $vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();
                $orders = Order::where('vendor_uuid', $vendor)->get();
                break;
            
            default:
                # code...
                $orders = Order::where('user_uuid', $user['uuid'])->get();
                break;
        }
        if(count($orders) > 0){
            return response()->json([
                'status' => 'success',
                'data' => $orders,
                'user_data' => $user,
                'message' => 'Orders for '.$account. ' retrieved successfully'
            ]);
        }else
        {
            return response()->json([
                'status' => 'success',
                'message' => 'You do not have any orders on ground'
            ]);
        }
        
    }

    public function addNotes(Request $request)
    {
        $user = Auth::user();
        $data = $request->All();
        $validator = Validator::make($data,[
            'order_uuid' => 'required',
            //'user_uuid' => 'required',
            'note' => 'required',
            //'media' => 'mime:'
        ]);
        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->errors(),
                'message' => 'Validation error'
            ],404);
        }
        //$vendor_data = Vendor::where('user_id',$user['uuid'])->first();
        //$vendor_uuid = $vendor_data['uuid'];
        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('user_id')->first();
        // $customer_data = User::where('u',$user['uuid'])->first();
        // $customer_uuid = $customer_data['uuid'];
        //$vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();
                //$orders = Order::where('vendor_uuid', $vendor)->get();

        $note = new Note;

        if($user['uuid'] == $vendor)
        {
            $note->uuid = Str::uuid();
            $note->order_uuid = $request->order_uuid;
            $note->user_uuid = $vendor;
            $note->user_type = 'Vendor';
            $note->note = $request->note;
            $note->media = $request->media;

            $note->save();
            return response()->json([
                'message' => 'Note sent',
                'data' => $data
            ],202);
        }
        else
        {
            $note->uuid = Str::uuid();
            $note->order_uuid = $request->order_uuid;
            $note->user_uuid = $user['uuid'];
            $note->user_type = 'Customer';
            $note->note = $request->note;
            $note->media = $request->media;

            $note->save();
            return response()->json([
                'message' => 'Note sent',
                'data' => $data
            ],202);
        }
    }

    public function getNotes($order_uuid)
    {

        if(Note::where('order_uuid',$order_uuid)->exists())
        {
            $notes = Note::where('order_uuid',$order_uuid)->get();
            return response()->json([
                'message' => 'Order notes',
                'data' => $notes
            ],202);
        }else
        {
            return response()->json([
                'Message' => 'This order has no note under it'
            ],200);
        }
    }

    public function updateOrderStatus(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        if(Vendor::where('user_id',$user['uuid'])->exists())
        {
            
            if(Order::where('uuid',$data['order_uuid'])->exists())
            {
                $order = Order::where('uuid',$data['order_uuid'])->first();
                //dd($order);
                $order->order_status = is_null($request->status) ? $order->order_status : $request->status;
                $order->save();
                //dd($order);
                $customer = User::where('uuid',$order['user_uuid'])->first();
                //dd($customer);
                $status = is_null($request->status) ? $order->order_status : $request->status;

                $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

		         $body = [
		         'Messages' => [
		         [
		             'From' => [
		                 'Email' => ENV('MAIL_FROM_ADDRESS'),
		                 'Name' => ENV('MAIL_FROM_NAME')
		             ],
		             'To' => [
		                 [
		                     'Email' => $customer->email,
		                     'Name' => $customer->username,
		                 ]
		             ],
		            
		             'TemplateID' => 2107244,
		             'TemplateLanguage' => true,
		            'Subject' => "",
		             'Variables' => [
		                 "username" => $customer->username,
		                 'status' => $status,
		                 'order_id' => $order->uuid
		             ]    
		         ]
		     ]
		 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);

                return response()->json([
                    'message' => 'Order status updated',
                    'data' => $order
                ],202);
            }else
            {
                return response()->json([
                    'message' => 'This order cant be found'
                ],404);
            }   
        }else
        {
            return response()->json([
                'message' => 'You are not authorized to do this'
            ],404);
        }
    }

    public function cancelOrder($order_uuid)
    {
        $user = Auth::user();
        
        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('user_id')->first();
        
        if(Order::where('uuid',$order_uuid)->exists()){
            $order = Order::where('uuid',$order_uuid);
            if($order['order_status'] == 'processing'){
                if($user['uuid'] == $vendor){
                    $order->order_status = "cancelled";
                    $customer = User::where('uuid',$order['user_uuid'])->first();
                
                
                $client = new Client([
             'base_uri' => 'https://api.mailjet.com/v3.1/',
         	]);

		         $body = [
		         'Messages' => [
		         [
		             'From' => [
		                 'Email' => ENV('MAIL_FROM_ADDRESS'),
		                 'Name' => ENV('MAIL_FROM_NAME')
		             ],
		             'To' => [
		                 [
		                     'Email' => $customer->email,
		                     'Name' => $customer->username,
		                 ]
		             ],
		            
		             'TemplateID' => 2107244,
		             'TemplateLanguage' => true,
		            'Subject' => "",
		             'Variables' => [
		                 "username" => $customer->username,
		                 'status' => "cancelled",
		                 'order_id' => $order->uuid
		             ]    
		         ]
		     ]
		 ];
         $response = $client->request('POST','send', [
             'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
             'json' => $body,
         ]);
                    return response()->json([
                        'message' => 'This order has been cancelled'
                    ],200);
                }else
                {
                    return response()->json([
                        'message'=> 'You cant cancel the order at this point'
                    ],200);
                }
            }else
            {
                $order->delete();
                return response()->json([
                    'message'=>'Order cancelled'
                ],200);
            }
        }else{
            return response()->json([
                'message' => 'This order cant be found'
            ],404);
        }
    }

    public function checkDate(Request $request)
    {
        $data = $request->all();
        $product = $data['products']; // take the product the user is trying to order
        $vendor_uuid = $product['data']['vendor']; // take the vendor associated with that product
        $checks = [
            'vendor' => $vendor_uuid,
            'unavailable_date' => $data['date_needed']
        ];

        if(Calendar::where($checks)->exists())
        {
            return response()->json([
                'message' => 'This vendor is unavailable'
            ],200);
        }else
        {
            return response()->json([
                'message' => 'This vendor is available'
            ],202);
        }
    } 

    public function getOrderByUuid($order_uuid)
    {
        if(Order::where('uuid',$order_uuid)->exists())
        {
            $order = Order::where('uuid',$order_uuid)->first();
            return response()->json([
                'message'=> 'Order successfully retrieved',
                'data'=>$order
            ],200);
        }else
        {
            return response()->json([
                'message' => 'This order cant be found'
            ],401);
        }

    }

    // public function testOrder($order_uuid)
    // {
    // 	 $user = Auth::user();
        
    //     $vendor_user = Vendor::where('user_id', $user['uuid'])->first();
        
    //     if(Order::where('uuid',$order_uuid)->exists()){
    //         $order = Order::where('uuid',$order_uuid)->first();

    //         $order_ = json_encode($order);

    //        //dd($order_->sales_price);
    //         	$client = new Client([
		  //            'base_uri' => 'https://api.mailjet.com/v3.1/',
		  //        	]);

			 //         $body = [
			 //         'Messages' => [
			 //         [
			 //             'From' => [
			 //                 'Email' => ENV('MAIL_FROM_ADDRESS'),
			 //                 'Name' => ENV('MAIL_FROM_NAME')
			 //             ],
			 //             'To' => [
			 //                 [
			 //                     'Email' => $vendor_user->business_email,
			 //                     'Name' => $vendor_user->name,
			 //                 ]
			 //             ],
			            
			 //             'TemplateID' => 2111910,
			 //             'TemplateLanguage' => true,
			 //            'Subject' => "New Listing Order Received",
			 //             'Variables' => [
			 //                "order_id" => $order->uuid,
			 //                "ref" => $order->listing_type,
			 //                "sales_price" => $order->sales_price
			 //            ]
			 //     ]
			 // ]
			 // ];
		  //        $response = $client->request('POST','send', [
		  //            'auth' => [ENV('MAILJET_APIKEY'), ENV('MAILJET_APISECRET')],
		  //            'json' => $body,
		  //        ]);
		        
    //         return response()->json([
    //         	'message' => 'New listing order recieved'
    //         ]);
    //     }
    // }



}
