<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str; 
use Illuminate\Http\Request;
use Validator;
use App\Category;
use App\Listing;
use App\Wishlist;
use App\Media;
use App\Rental_option;
use App\Product_requirement;
use App\Pricing_package;
use App\Product_faq;
use App\Event_type;
use App\Vendor;
use App\Listing_attribute;
use App\Product_gallery;
use App\User;
use JD\Cloudder\Facades\Cloudder;
use Igaster\LaravelCities\Geo;


class ServiceController extends Controller
{
    //
    public function addCategory(Request $request) {

        $admin = Auth()->user();
    
        $data = $request->all();
        $validator = Validator::make($request->all(), [ 
            'name' => 'required'
        ]);
        
        if ($validator->fails())
         { 
            return response()->json(['error'=>$validator->errors()], 401);  
        }
        if($admin['role_id'] == 1)
        {
            if(Category::where('name', $data['name'])->count() > 0) 
        {
            return response()->json([
                'status' => 'failed',
                'message' => 'Category already exists'
            ], 401);
        }
        
        if(Category::create($data))
         {
             return response()->json([
                'status' => 'success',
                'message' => 'Category created successfully',
                'categories' => Category::get()
           ], 200);
        }    
        }else
        {
            return response()->json([
                'message'=>'Unauthorized User'
            ]);
        }
        
    }
    
    public function addRentalOption(Request $request) {
        $data = $request->all();
        $validator = Validator::make($request->all(), [ 
            'option' => 'required'
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);  
        }
        
        if(Rental_option::where('option', $data['option'])->count() > 0) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Rental option already exists'
            ], 401);
        }
        
        if(Rental_option::create($data)) {
             return response()->json([
                'status' => 'success',
                'message' => 'Rental option created successfully',
                'rental_options' => Rental_option::get()
           ], 200);
        }
        
        
    }
    
    public function fetchRentalOption() {
        $options = Rental_option::get();
        
        $chunk = [];
        
        // foreach($categories as $category) {
        //     $children = Category::where('parent', $category['uuid'])->get();
        //     $category['children'] = $children;
            
        //     if($category['parent'] == null) {
        //         array_push($chunk, $category);
        //     }
        // }
        
        return response()->json([
            'status' => 'success',
            'rental_options' => $options
        ], 200);
        
    }
    
    public function fetchCategory() {
        $categories = Category::get();
        
        $chunk = [];
        
        foreach($categories as $category) {
            $children = Category::where('parent', $category['uuid'])->get();
            $category['children'] = $children;
            
            if($category['parent'] == null) {
                array_push($chunk, $category);
            }
        }
        
        return response()->json([
            'status' => 'success',
            'categories' => $categories
        ], 200);
        
    }
    
    public function addListing(Request $request, $status) {
        $data = $request->all();
        $user = Auth::user();
        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();



        /***
         * Re-manipulation starts here
         */
        if($status == 'publish' && $data['listing_id'] == null) {
             //Publish gallery
            $product_galleries = [];
            $options = array();
            foreach($data['gallery'] as $gallery) {  
                if(Cloudder::upload($gallery, $options)) {
                    $gallery_result = Cloudder::getResult();
                    
                    
                    $product_gallery = $gallery_result['secure_url'];
                    
                    array_push($product_galleries, $product_gallery);
                
                };
            }

            $data['gallery'] = $product_galleries;
            $data['listing_type'] = $data['service_type'];
            $data['service_type'] = $data['event_type'];
            $data['sku'] = 'SKU'.hexdec(uniqid());
            unset($data['event_type']);
            unset($data['listing_id']);
            $data['vendor'] = $vendor;

            $product_id = Listing::create($data)->uuid;

            return response()->json([
                'status' => 'success',
                'message' => 'Listing added successfully',
                'product_id' => $product_id    

            ], 200);

        }


        /***
         * Re-manipulation ends here
         */


        // $faqs = $data['faq'];
        // $attributes = $data['product_attributes'];
        // $galleries = $data['gallery'];
        // $requirements = $data['requirements'];
        // $pricing_packages = $data['pricing_packages'];

        // $product_info = $data;
        // $product_info['service_type'] = $data['event_type'];
        // $product_info['vendor'] = $vendor;

        // unset($product_info['faq']);
        // unset($product_info['product_attributes']);
        // unset($product_info['gallery']);
        // unset($product_info['requirements']);
        // unset($product_info['pricing_packages']);
        // unset($product_info['listing_id']);

        // Create the product

        // if($status == 'publish') {
        //     $product_id = Listing::create($product_info)->uuid;
        //     //publish product faq
        //     $product_faqs = [];
        //     foreach($faqs as $faq) {
        //         $faq['vendor'] = $vendor;
        //         $faq['service'] = $product_id;
                
        //         array_push($product_faqs, $faq);
        //     } 
        //     Product_faq::insert($product_faqs);
            
        //     //publish attributes 
        //     $product_attributes = [];
        //     foreach($attributes as $attribute) {
        //         $attribute['vendor_id'] = $vendor;
        //         $attribute['listing_id'] = $product_id;
                
        //         array_push($product_attributes, $attribute);
        //     }
        //     Listing_attribute::insert($product_attributes);
            
        //     //Publish gallery
        //     $product_galleries = [];
        //     $options = array();
        //     foreach($galleries as $gallery) {  
        //         if(Cloudder::upload($gallery, $options)) {
        //             $gallery_result = Cloudder::getResult();
        //             $media_data = array(
        //             'url' => $gallery_result['secure_url'],
        //             'content_type' => 'product_image_'.$product_id,
        //             'user' => $user['uuid'],
        //             'media_type' => $gallery_result['resource_type'],
        //             'size' => $gallery_result['bytes'],
        //             'storage_id' => $gallery_result['public_id'],
        //             'width' => $gallery_result['width'],
        //             'height' => $gallery_result['height'],
        //             'alt' => $data['title'].' product image'
                    
        //             );
                    
        //             $product_gallery['media_url'] = Media::create($media_data)->id;
        //             $product_gallery['vendor'] = $vendor;
        //             $product_gallery['service'] = $product_id;
                    
        //             array_push($product_galleries, $product_gallery);
                
        //         };
        //     }
        //     Product_gallery::insert($product_galleries);
            
        //     //Publish requirements
        //     $product_requirements = [];
        //     foreach($requirements as $requirement) {
        //         $r['requirement'] = $requirement;
        //         $r['service'] = $product_id;
        //         $r['vendor'] = $vendor;
                
        //         array_push($product_requirements, $r);
        //     }
        //     Product_requirement::insert($product_requirements);
            
        //     //Publish packages
        //     $product_packages = [];
        //     foreach($pricing_packages as $package) {
        //         $package['service'] = $product_id;
        //         $package['vendor'] = $vendor;
                
        //         array_push($product_packages, $package);
        //     }
        //     Pricing_package::insert($product_packages);
            
        //     return response()->json([
        //         'status' => 'success',
        //         'message' => 'Listing added successfully',
        //         'id' => $product_id    

        //     ], 200);
        // }
        if($status == 'publish' && $data['listing_id'] !== null) {
            $data['status'] = 0;
            $product_id = $data['listing_id'];
            $product_galleries = [];
            $options = array();
            foreach($data['gallery'] as $gallery) {  
                if(Cloudder::upload($gallery, $options)) {
                    $gallery_result = Cloudder::getResult();
                    
                    
                    $product_gallery = $gallery_result['secure_url'];
                    
                    array_push($product_galleries, $product_gallery);
                
                };
            }

            $data['gallery'] = $product_galleries;
            $data['listing_type'] = $data['service_type'];
            $data['service_type'] = $data['event_type'];
            unset($data['event_type']);
            unset($data['listing_id']);
            $data['vendor'] = $vendor;

            // $product_id = Listing::create($data)->uuid;
            Listing::where('uuid', $product_id)->update($data);

            return response()->json([
                'status' => 'success',
                'message' => 'Listing published successfully',
                'product_id' => $product_id    

            ], 200);
        }

        if($status == 'draft' && $data['listing_id'] == null) {
            $data['status'] = 4;

            $product_galleries = [];
            $options = array();
            foreach($data['gallery'] as $gallery) {  
                if(Cloudder::upload($gallery, $options)) {
                    $gallery_result = Cloudder::getResult();
                    
                    
                    $product_gallery = $gallery_result['secure_url'];
                    
                    array_push($product_galleries, $product_gallery);
                
                };
            }

            $data['gallery'] = $product_galleries;
            $data['listing_type'] = $data['service_type'];
            $data['service_type'] = $data['event_type'];
            unset($data['event_type']);
            unset($data['listing_id']);
            $data['vendor'] = $vendor;

            $product_id = Listing::create($data)->uuid;

            return response()->json([
                'status' => 'success',
                'message' => 'Listing saved successfully',
                'product_id' => $product_id    

            ], 200);
        }

        if($status == 'draft' && $data['listing_id'] != null) {



            $product_id = $data['listing_id'];

            $product_info['status'] = 4;

            $product_galleries = [];
            $options = array();
            foreach($data['gallery'] as $gallery) {  
                if(Cloudder::upload($gallery, $options)) {
                    $gallery_result = Cloudder::getResult();
                    
                    
                    $product_gallery = $gallery_result['secure_url'];
                    
                    array_push($product_galleries, $product_gallery);
                
                };
            }

            $data['gallery'] = $product_galleries;
            $data['listing_type'] = $data['service_type'];
            $data['service_type'] = $data['event_type'];
            unset($data['event_type']);
            unset($data['listing_id']);
            $data['vendor'] = $vendor;
            
            Listing::where('uuid', $product_id)->update($data);

            return response()->json([
                'status' => 'success',
                'message' => 'Listing saved successfully',
                'product_id' => $product_id    

            ], 200);

            // $product_id = $data['listing_id'];
            // Listing::where('uuid', $data['listing_id'])->update($product_info);

            // //publish product faq
            // $product_faqs = [];
            // foreach($faqs as $faq) {
            //     $faq['vendor'] = $vendor;
            //     $faq['service'] = $product_id;
                
            //     array_push($product_faqs, $faq);
            // } 

            // Product_faq::where('service', $product_id)->delete();
            // Product_faq::insert($product_faqs);
            
            // //publish attributes 
            // $product_attributes = [];
            // foreach($attributes as $attribute) {
            //     $attribute['vendor_id'] = $vendor;
            //     $attribute['listing_id'] = $product_id;
                
            //     array_push($product_attributes, $attribute);
            // }

            // Listing_attribute::where('service', $product_id)->delete();
            // Listing_attribute::insert($product_attributes);
            
            // //Publish gallery
            // $product_galleries = [];
            // $options = array();
            // foreach($galleries as $gallery) {  
            //     if(Cloudder::upload($gallery, $options)) {
            //         $gallery_result = Cloudder::getResult();
            //         $media_data = array(
            //         'url' => $gallery_result['secure_url'],
            //         'content_type' => 'product_image_'.$product_id,
            //         'user' => $user['uuid'],
            //         'media_type' => $gallery_result['resource_type'],
            //         'size' => $gallery_result['bytes'],
            //         'storage_id' => $gallery_result['public_id'],
            //         'width' => $gallery_result['width'],
            //         'height' => $gallery_result['height'],
            //         'alt' => $data['title'].' product image'
                    
            //         );
                    
            //         $product_gallery['media_url'] = Media::create($media_data)->id;
            //         $product_gallery['vendor'] = $vendor;
            //         $product_gallery['service'] = $product_id;
                    
            //         array_push($product_galleries, $product_gallery);
                
            //     };
            // }

            // Product_gallery::where('service', $product_id)->delete();
            // Product_gallery::insert($product_galleries);
            
            // //Publish requirements
            // $product_requirements = [];
            // foreach($requirements as $requirement) {
            //     $r['requirement'] = $requirement;
            //     $r['service'] = $product_id;
            //     $r['vendor'] = $vendor;
                
            //     array_push($product_requirements, $r);
            // }
            // Product_requirement::where('service', $product_id)->delete();
            // Product_requirement::insert($product_requirements);
            
            // //Publish packages
            // $product_packages = [];
            // foreach($pricing_packages as $package) {
            //     $package['service'] = $product_id;
            //     $package['vendor'] = $vendor;
                
            //     array_push($product_packages, $package);
            // }
            // Pricing_package::where('service', $product_id)->delete();
            // Pricing_package::insert($product_packages);
            
            // return response()->json([
            //     'status' => 'success',
            //     'message' => 'Listing saved successfully',
            //     'id' => $product_id    

            // ], 200);
        }
        
        
        
    }
    
    public function getListing() {
        $listing = Listing::where('status', 1)->inRandomOrder()->paginate(50);
        
        $chunk = [];
        
        foreach($listing as $list) {
            $list['vendor_details'] = Vendor::where('uuid', $list['vendor'])->first();
            $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            $type = [];
            $package_prices = [];
            foreach($list['service_type'] as $types) {
                $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
                array_push($type, $typeMember);
            }
            
            foreach(Geo::getByIds([$list['country']]) as $country){
                $list['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$list['city']]) as $city){
                $list['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$list['region']]) as $region){
                $list['region'] = $region['name'];
            }

            
            
            $list['sales_price'] = (int)$list['total_price'] * (int)$list['discount'] / 100;
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
            $list['unit_option'] = '';
            // $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            // $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            // $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            // $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            // $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();
            foreach($list['attributes'] as $attr) {
                $attr['values'] = (explode(",",$attr['values']));
            }
            $list['service_type'] = $type;

            foreach($list['pricing_package'] as $p_price) {
                array_push($package_prices, (int)$p_price['price']);
            }

            sort($package_prices);
            $list['pricing_package_starts_from'] = count($package_prices) > 0 && $list['listing_type'] == 'service' ? $package_prices[0]: $list['sales_price'];
            // $list['country']
            array_push($chunk, $list);
        }
        return response()->json([
            'status' => 'success',
            'data' => $chunk,
            'message' => 'All listings'
        ], 200);
        
    }


    public function deleteListing($uuid) {

        $user = Auth::user();
        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();

        try {
            //code...
            Listing::where('uuid', $uuid)->where('vendor', $vendor)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Listing deleted successfully',
                'vendor' => $vendor. ' '. $uuid
            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'status' => 'failed',
                'message' => 'Listing cannot be deleted',
                'log' => $th->getMessage()
            ], 200);
        }

        
    }
    
    public function getDealListing() {
        $listing = Listing::where('status', 1)->where('discount', '>', 0)->inRandomOrder()->get();
        
        $chunk = [];

        
        
        foreach($listing as $list) {
            $list['vendor_details'] = Vendor::where('uuid', $list['vendor'])->first();
            $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            $type = [];
            $package_prices = [];
            foreach($list['service_type'] as $types) {
                $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
                array_push($type, $typeMember);
            }
            
            foreach(Geo::getByIds([$list['country']]) as $country){
                $list['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$list['city']]) as $city){
                $list['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$list['region']]) as $region){
                $list['region'] = $region['name'];
            }
            $list['sales_price'] = (int)$list['total_price'] * (int)$list['discount'] / 100;
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
            $list['unit_option'] = '';
            // $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            // $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            // $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            // $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            // $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();
            foreach($list['attributes'] as $attr) {
                $attr['values'] = (explode(",",$attr['values']));
            }
            $list['service_type'] = $type;

            foreach($list['pricing_package'] as $p_price) {
                array_push($package_prices, (int)$p_price['price']);
            }

            sort($package_prices);
            $list['pricing_package_starts_from'] =  count($package_prices) > 0 ? $package_prices[0]: null;
            // $list['country']
            array_push($chunk, $list);
        }
        return response()->json([
            'status' => 'success',
            'data' => $chunk,
            'message' => 'All listings'
        ], 200);
        
    }
    
    public function myListing() {
        $user = Auth::user();
        $vendor_id = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();
        $listing = Listing::where('vendor', $vendor_id)->latest()->get();
        
        $chunk = [];
        
        foreach($listing as $list) {
            $list['vendor_details'] = User::where('uuid', $list['vendor'])->first();
            $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            $type = [];
            foreach($list['service_type'] as $types) {
                $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
                array_push($type, $typeMember);
            }
            
            foreach(Geo::getByIds([$list['country']]) as $country){
                $list['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$list['city']]) as $city){
                $list['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$list['region']]) as $region){
                $list['region'] = $region['name'];
            }
            
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
            
            $list['unit_option'] = '';
            // $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            // $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            // $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            // $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            // $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();
            $list['all_service_type'] = $type;
            array_push($chunk, $list);
        }
        
        return response()->json([
            'status' => 'success',
            'data' => $chunk,
            'message' => 'My listings'
        ], 200);
        
    }
    
    public function vendorListing($product_owner) {
        $listing = Listing::where('status', 1)->where('vendor', $product_owner)->latest()->get();
        
        $chunk = [];
        
        foreach($listing as $list) {
            $list['vendor_details'] = User::where('uuid', $list['vendor'])->first();
            $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            $type = [];
            foreach($list['service_type'] as $types) {
                $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
                array_push($type, $typeMember);
            }
            
            foreach(Geo::getByIds([$list['country']]) as $country){
                $list['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$list['city']]) as $city){
                $list['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$list['region']]) as $region){
                $list['region'] = $region['name'];
            }
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
            $list['unit_option'] = '';
            // $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            // $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            // $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            // $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            // $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();
            // foreach($list['attributes'] as $attr) {
            //     $attr['values'] = (explode(",",$attr['values']));
            // }
            $list['all_service_type'] = $type;
            array_push($chunk, $list);
        }
        
        return response()->json([
            'status' => 'success',
            'data' => $chunk,
            'message' => 'My listings'
        ], 200);
        
    }
    
    public function getListingBySlug($slug) {
        $user = Auth::user();
        $list = Listing::where('slug', $slug)->first();
        $list['vendor_details'] = Vendor::where('uuid', $list['vendor'])->first();
        foreach(Geo::getByIds([$list['vendor_details']['country']]) as $country){
            $list['vendor_details']['country']= $country['name'];
        }
        // $list['vendor_details']['business_logo'] = Media::where('content_type', 'business_logo')->where('id', $list['vendor_details']['business_logo'])->pluck('url')->first();
        $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
        $type = [];
            foreach($list['service_type'] as $types) {
                $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
                array_push($type, $typeMember);
            }
            
            foreach(Geo::getByIds([$list['country']]) as $country){
                $list['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$list['city']]) as $city){
                $list['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$list['region']]) as $region){
                $list['region'] = $region['name'];
            }
            
            $list['unit_option'] = '';
            // $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            // $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            // $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            // $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            // $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();
            // foreach($list['attributes'] as $attr) {
            //     $attr['values'] = (explode(",",$attr['values']));
            // }
            $list['all_service_type'] = $type;
            $list['sales_price'] = (int)$list['total_price'] * (int)$list['discount'] / 100;
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
        
        
        return response()->json([
            'status' => 'success',
            'data' => $list,
            'message' => 'Listing details retrieved'
        ], 200);
        
    }

    public function getListingById($id) {
        $user = Auth::user();
        $list = Listing::where('uuid', $id)->first();
        
        
        return response()->json([
            'status' => 'success',
            'data' => $list,
            'message' => 'Listing retrieved retrieved'
        ], 200);
        
    }

    public function getManyListingsById(Request $request) {
        $data = $request->all();

        $chunk = [];

        foreach($data as $dat) {
            $list = gettype($dat) == 'string' ? Listing::where('uuid', $dat)->first() :Listing::where('uuid', $dat['id'])->first();
            $list['vendor_details'] = Vendor::where('uuid', $list['vendor'])->first();
            $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            $type = [];
            $package_prices = [];
            foreach($list['service_type'] as $types) {
                $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
                array_push($type, $typeMember);
            }
            
            foreach(Geo::getByIds([$list['country']]) as $country){
                $list['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$list['city']]) as $city){
                $list['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$list['region']]) as $region){
                $list['region'] = $region['name'];
            }

            
            
            $list['sales_price'] = (int)$list['total_price'] * (int)$list['discount'] / 100;
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
            $list['unit_option'] = '';
            // $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            // $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            // $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            // $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            // $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();
            $list['all_service_type'] = $type;

            foreach($list['pricing_package'] as $p_price) {
                array_push($package_prices, (int)$p_price['price']);
            }

            sort($package_prices);
            $list['pricing_package_starts_from'] = count($package_prices) > 0 && $list['listing_type'] == 'service' ? $package_prices[0]: $list['sales_price'];
            // $list['country']
            array_push($chunk, $list);
        }

        return response()->json([
            'status' => 'success',
            'data' => $chunk,
            'message' => 'All listings generated successfully'
        ], 200);

        
    }
    
    public function createDeal(Request $request) {
        $user = Auth::user();
        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();
        
        $data = $request->all();
        
        $deal = Array(
                'discount' => $data['discount'] < 100 ? $data['discount'] : 0,
                'deals' => true
            );
            
        Listing::where('vendor', $vendor)->where('uuid', $data['listing'])->update($deal);
        
        return response()->json([
            'status' => 'success',
            'data' => $data,
            'message' => 'Deals created successfully'
        ], 200);
    }
    
    public function deleteDeal(Request $request) {
        $user = Auth::user();
        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();
        
        $data = $request->all();
        
        $deal = Array(
                'discount' => 0,
                'deals' => false
            );
            
        Listing::where('vendor', $vendor)->where('uuid', $data['listing'])->update($deal);
        
        return response()->json([
            'status' => 'success',
            'data' => $data,
            'message' => 'Deals deleted successfully'
        ], 200);
    }
    
    public function getDeals() {
        $user = Auth::user();
        $vendor = Vendor::where('user_id', $user['uuid'])->pluck('uuid')->first();
            
        $listing = Listing::where('vendor', $vendor)->where('deals', '1')->where('discount','>', 0)->get();
        
        $all = [];
        
        foreach($listing as $list) {
            $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->pluck('url')->first();
            $list['sales_price'] = (int)$list['total_price'] * (int)$list['discount'] / 100;
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
        }
        
        
        return response()->json([
            'status' => 'success',
            'data' => $listing,
            'message' => 'Deals retrieved successfully'
        ], 200);
    }

public function getVendorListingsbyUsername($username)
{
    if(Vendor::where('username',$username)->exists())
    {
        $listings = Listing::where('vendor',$username)->get();

        if(count($listings) > 0)
        {
            return response()->json([
            'message' => 'Vendors Listing',
            'data' => $listings
            ],202);
        }else
        {
            return response()->json([
                'message' => 'This vendor has no listing'
            ],200);
        }
        
    }else{
        return response()->json([
            'message' => 'This vendor does not exist'
        ],404);
    }
}

public function getVendorListingsbyId($id)
{
    if(Vendor::where('uuid',$id)->exists())
    {
        $vendor = Vendor::where('uuid',$id)->first();
        $listings = Listing::where('vendor',$vendor->username)->get();

        if(count($listings) > 0)
        {
            return response()->json([
                'message' => 'Vendors listing',
                'data' => $listings
            ],202);
        }else
        {
            return response()->json([
                'message' => 'This vendor has no listing',

            ],200);
        }

    }else{
        return response()->json([
            'message' => 'This vendor does not exist'
        ],404);
    }
}
    
}

