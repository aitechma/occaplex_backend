<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Http\Request;

use App\StaticPage;

class StaticPagesController extends Controller
{
    //

    public function getPages($name) {
        $result = StaticPage::where('name', $name)->first();

        return response()->json([
            'status' => $result == null ? 'error' : 'success',
            'message' => $result == null ? 'Page not found' : 'Page retrieved successfully',
            'pages' => $result
       ], 200);
    }
}
