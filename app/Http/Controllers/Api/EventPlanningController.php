<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Event;
use App\Pricing_package;
use App\Product_requirement;
use App\Product_faq;
use App\Event_type;
use App\Media;
use App\Event_checklist;
use App\Rental_option;
use App\Category;
use App\Listing;
use App\Listing_attribute;
use App\Product_gallery;
use App\Event_planning;
use App\Event_planning_group;
use App\Event_supplier;
use App\Group;
use App\Group_member;
use App\Vendor;
use Illuminate\Http\Request;
use Igaster\LaravelCities\Geo;

class EventPlanningController extends Controller
{
    //

    public function getGroups() {
        $user = Auth::user();
        $groups = Group::where('creator', $user['uuid'])->get();
        return response()->json([
            'status' => 'success',
            'message' => 'Group retrieved successfully',
            'data' => $groups
        ], 200);

    }

    public function createGroup(Request $request) {
        $user = Auth::user();
        $data = $request->all();
        $data['creator'] = $user['uuid'];

        $members = $data['members'];

        unset($data['members']);
        
        $group = Group::create($data);

        foreach($members as $member) {

            if($member !== $user['email']) {
                $memberVariable = array(
                    "email" => $member,
                    "approved" => 0,
                    "user" => User::where('email', $member)->pluck('uuid')->first(),
                    "group" => $group['uuid']
                    // "event_id" => $event_planning['uuid']
                );
    
                if(Group_member::create($memberVariable)) {
                    //Send email here
                }
            }
            
        }

        $adminVariable = array(
            "email" => $user['email'],
            "approved" => 1,
            "user" => $user['uuid'],
            "group" => $group['uuid'],
            // "event_id" => $event_planning['uuid'],
            "role" => "admin"
        );

        if(Group_member::create($adminVariable)) {
            //Send email here
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Group created successfully',
            'data' => $group
        ], 200);

    }

    public function createEvent(Request $request) {
        $data = $request->all();
        $user = Auth::user();

        $data['user_id'] = $user['uuid'];

        $event = Event_planning::create($data);

        return response()->json([
            'status' => 'success',
            'message' => 'Event created successfully',
            'data' => $event
        ], 200);

    }

    public function getEventById($uuid) {
        $user = Auth::user();
        $event = Event_planning::where('uuid', $uuid)->where('user_id', $user['uuid'])->first();
        return response()->json([
            'status' => 'success',
            'message' => 'Event retrieved successfully',
            'data' => $event
        ], 200);
    }

    public function updateEventById(Request $request, $uuid) {
        $user = Auth::user();
        Event_planning::where('uuid', $uuid)->where('user_id', $user['uuid'])->update($request->all());
        return response()->json([
            'status' => 'success',
            'message' => 'Event updated successfully',
            'data' => Event_planning::where('uuid', $uuid)->where('user_id', $user['uuid'])->first()
        ], 200);
    }

    public function submit(Request $request) {
        $data = $request->all();
        
        $authUser = Auth::user();
        
        $data['user_id'] =  $authUser['uuid'];
        $data['paid'] = false;
        $data['status'] = 'active';
        
        // $groupData = $data['plan_as_group'] == true ? $data['planning_group']: [];
        
        $eventPlanningData = Array(
                "event_title" => $data['event_title'],
                "user_id" => $data['user_id'],
                "plan_as_group" => $data['plan_as_group'],
                "do_it" => $data['do_it'],
                "event_type" => $data['event_type'],
                "date" => $data['date'],
                "address" => $data['address'],
                "city" => $data['city'],
                "country" => $data['country'],
                "population" => $data['population'],
                "event_theme" => $data['event_theme'],
                "primary_color" => $data['primary_color'],
                "secondary_color" => $data['secondary_color'],
                "event_activities" => $data['event_activities'],
                "time" => $data['time']
                // "privacy" => $data['privacy'],
                // "total_price" => $data['total_price'],
                // "paid" => (int)$data['event_price'] > 0 ? true : false
                
            );
            
        $event_planning = Event_planning::create($eventPlanningData); // create the event planning


        if($data['plan_as_group'] == true) {
            $createGroup = array(
                "creator" => $data['user_id'],
                "event_id" => $event_planning['uuid'],
                "title" => $data['group_details']['title'],
                "description" => $data['group_details']['description']
            );
            $group = Group::create($createGroup); //create the event group
        }

        foreach($data['event_checklist'] as $checklist) {
            $checklistVariable = array(
                "category_id" => $checklist['category'],
                "budget" => $checklist['budget'],
                "user_id" => $data['user_id'],
                "event_id" => $event_planning['uuid'],
                "group_id" => $data['plan_as_group'] == true ? $group['uuid'] : null
            );

            Event_checklist::create($checklistVariable);
        }

        foreach($data['event_suppliers'] as $supplier) {
            $supplierVariable = array(
                "listing_id" => $supplier['id'],
                "event_supplier_features" => ['attributes' => $supplier['attributes'], 'price' => $supplier['price'], 'quantity' => $supplier['quantity']],
                "user_id" => $data['user_id'],
                "event_id" => $event_planning['uuid'],
                "group_id" => $data['plan_as_group'] == true ? $group['uuid'] : null
            );

            Event_supplier::create($supplierVariable);
        }

        if($data['plan_as_group'] == true) {
            foreach($data['planning_group'] as $member) {

                if($member !== $authUser['email']) {
                    $memberVariable = array(
                        "email" => $member,
                        "approved" => 0,
                        "user" => User::where('email', $member)->pluck('uuid')->first(),
                        "group_id" => $data['plan_as_group'] == true ? $group['uuid'] : null,
                        "event_id" => $event_planning['uuid']
                    );
        
                    if(Group_member::create($memberVariable)) {
                        //Send email here
                    }
                }
                
            }

            $adminVariable = array(
                "email" => $authUser['email'],
                "approved" => 1,
                "user" => $authUser['uuid'],
                "group_id" => $data['plan_as_group'] == true ? $group['uuid'] : null,
                "event_id" => $event_planning['uuid'],
                "role" => "admin"
            );

            if(Group_member::create($adminVariable)) {
                //Send email here
            }


        }

        
        
        
            
        
        // if($data['do_it'] == true) {
        //    foreach($data['event_checklist'] as $ch) {
        //        $eventChecklists = Array(
        //            "event_id" => $event_planning['uuid'],
        //            "user_id" => $data['user_id'],
        //            "listing_id" => $ch,
        //            "vendor_id" => Listing::where('uuid', $ch)->pluck('vendor')->first(),
        //            "list_price" => Listing::where('uuid', $ch)->pluck('total_price')->first()
        //            );
                   
        //         Event_checklist::create($eventChecklists);
                   
                
        //    }
        // }
        
        // $event_planning = Event_planning::create($data);
        
        // $privacy = $data['privacy'];
        
        // if($privacy == true) {
        //     $eventsData = array(
        //         "title" => $data['event_title'],
        //         "location" =>  $data['address'],
        //         "description" =>  $data['about_event'],
        //         "capacity" =>  $data['population'],
        //         "date" => $data['date'],
        //         "price" =>  $data['event_price'],
        //         "event_planning" => $event_planning['uuid']
        //     );
            
        //     Event::create($eventsData);
                
        // }
        
        
        
        return response()->json([
                'status' => 'success',
                'message' => 'Event planning created successfully',
                'data' => $event_planning,
                'group' => $data['plan_as_group'] == true ? $group : null
        ], 200);
        
        
        
        
        // if($)
        
    }
    
    public function checklist(Request $request) {
        $data = $request->all();
        $authUser = Auth::user();
        
        $event_type = $data['event_type'];
        
        $listing = Listing::where('service_type','like', "%{$event_type}%")->get();
        
        $filtered = [];
        
        $categories = Category::pluck('uuid');
        
        foreach($categories as $category) {
            $ch = Listing::where('category', $category)->where('service_type','like', "%{$event_type}%")->inRandomOrder()->first();
            array_push($filtered, $ch['category']);
        }
        
        $chunk = [];
        
        foreach ($filtered as $f) {
            if($f !== null) {
                $f = Category::where('uuid', $f)->first();
                array_push($chunk, $f);
            }
        }
        
        return response()->json([
                'status' => 'success',
                'message' => 'Checklists generated successfully',
                'data' => $chunk
        ], 200);
        
        
    }
    
    public function recommendVendors(Request $request) {
        $data = $request->all();
        $chunk = [];
        $category_budget = [];
        $total_price = 0;

        foreach ($data as $key => $value) {
            if(gettype($value) === 'integer' || gettype($value) === 'double'){
                $selectedData = array("category" => $key, "budget" => $value, "name" => Category::where('uuid', $key)->pluck('name')->first());
            array_push($category_budget, $selectedData);
            }
            
        }


        $merger = array();
        
        foreach($category_budget as $d) {
            
            // $list = Listing::where('category', $d['category'])->where('total_price', '<', (int)$d['budget'])->inRandomOrder()->first();
            $listing = Listing::where('category', $d['category'])->inRandomOrder()->get();
            // if($list !== null) {
            //     $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            //     $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            
            // array_push($chunk, $list);
            // }

            foreach ($listing as $clist) {
                # code...
                array_push($merger, $clist);
            }

            
        }

        foreach($merger as $list) {
            // $list['vendor_details'] = Vendor::where('uuid', $list['vendor'])->first();
            // $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            // $type = [];
            // $package_prices = [];
            // foreach($list['service_type'] as $types) {
            //     $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
            //     array_push($type, $typeMember);
            // }
            
            // foreach(Geo::getByIds([$list['country']]) as $country){
            //     $list['country'] = $country['name'];
            // }
            // foreach(Geo::getByIds([$list['city']]) as $city){
            //     $list['city'] = $city['name'];
            // }
            // foreach(Geo::getByIds([$list['region']]) as $region){
            //     $list['region'] = $region['name'];
            // }

            
            
            // $list['sales_price'] = (int)$list['total_price'] * (int)$list['discount'] / 100;
            // $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
            // $list['unit_option'] = '';
            // $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            // $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            // $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            // $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            // $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();

            // foreach($list['attributes'] as $attr) {
            //     $attr['values'] = (explode(",",$attr['values']));
            // }
            // $list['service_type'] = $type;

            // foreach($list['pricing_package'] as $p_price) {
            //     array_push($package_prices, (int)$p_price['price']);
            // }

            // sort($package_prices);
            // $list['pricing_package_starts_from'] = count($package_prices) > 0 && $list['listing_type'] == 'service' ? $package_prices[0]: $list['sales_price'];
            // // $list['country']
            $list['vendor_details'] = Vendor::where('uuid', $list['vendor'])->first();
            $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            $type = [];
            $package_prices = [];
            foreach($list['service_type'] as $types) {
                $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
                array_push($type, $typeMember);
            }
            
            foreach(Geo::getByIds([$list['country']]) as $country){
                $list['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$list['city']]) as $city){
                $list['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$list['region']]) as $region){
                $list['region'] = $region['name'];
            }

            
            
            $list['sales_price'] = (int)$list['total_price'] * (int)$list['discount'] / 100;
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
            $list['unit_option'] = '';
            // $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            // $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            // $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            // $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            // $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();
            foreach($list['attributes'] as $attr) {
                $attr['values'] = (explode(",",$attr['values']));
            }
            $list['service_type'] = $type;

            foreach($list['pricing_package'] as $p_price) {
                array_push($package_prices, (int)$p_price['price']);
            }

            sort($package_prices);
            $list['pricing_package_starts_from'] = count($package_prices) > 0 && $list['listing_type'] == 'service' ? $package_prices[0]: $list['sales_price'];
            // $list['country']
            array_push($chunk, $list);
        }
        return response()->json([
            'status' => 'success',
            'data' => $chunk,
            'checklist' => $category_budget,
            'message' => 'Vendor checklists generated successfully'
        ], 200);

        // return response()->json(["data" => $merger]);
        
        // return response()->json([
        //         'status' => 'success',
        //         'message' => 'Vendor checklists generated successfully',
        //         'data' => $chunk,
        //         'total_price' => $total_price
        // ], 200);
    }

    
    public function myEventPlanning() {
        $authUser = Auth::user();
        
        $eventplanning = Event_planning::where('user_id', $authUser['uuid'])->latest()->get();

        
        
        foreach($eventplanning as $ev) {
            $ev['event_type'] = Event_type::where('uuid', $ev['event_type'])->pluck('event_type')->first();
            foreach(Geo::getByIds([$ev['country']]) as $country){
                $ev['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$ev['city']]) as $city){
                $ev['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$ev['region']]) as $region){
                $ev['region'] = $region['name'];
            }

            $group_members = Group_member::where('event_id', $ev['uuid'])->get();

            $planning_members = [];

            foreach ($group_members as $mem) {
                # code...
                $mem_details = User::where('email', $mem['email'])->first();

                array_push($planning_members, $mem_details);
            }

            $ev['group_members'] = $planning_members;
            $ev['suppliers'] = Event_supplier::where('event_id', $ev['uuid'])->pluck('listing_id');
            $ev['supplier_features'] = Event_supplier::where('event_id', $ev['uuid'])->pluck('event_supplier_features');


        }
        
        return response()->json([
                'status' => 'success',
                'message' => 'My event planning retrieved successfully',
                'data' => $eventplanning
        ], 200);
    }
    
    public function myEventPlanningProducts($id) {
        $authUser = Auth::user();
        
        $products = Event_checklist::where('event_id', $id)->where('user_id', $authUser['uuid'])->get();
        
        $chunk = [];
        
        foreach($products as $pr) {
            $listing = Listing::where('uuid', $pr['listing_id'])->get();
            foreach($listing as $list) {
            $list['vendor_details'] = User::where('uuid', $list['vendor'])->first();
            $list['category'] = Category::where('uuid', $list['category'])->pluck('name')->first();
            $type = [];
            foreach($list['service_type'] as $types) {
                $typeMember = Event_type::where('uuid', $types)->pluck('event_type')->first();
                
                array_push($type, $typeMember);
            }
            
            foreach(Geo::getByIds([$list['country']]) as $country){
                $list['country'] = $country['name'];
            }
            foreach(Geo::getByIds([$list['city']]) as $city){
                $list['city'] = $city['name'];
            }
            foreach(Geo::getByIds([$list['region']]) as $region){
                $list['region'] = $region['name'];
            }
            $list['sales_price'] = (int)$list['total_price'] * (int)$list['discount'] / 100;
            $list['sales_price'] = (int)$list['total_price'] - $list['sales_price'] ;
            $list['unit_option'] = '';
            $list['gallery'] = Media::where('content_type', 'product_image_'.$list['uuid'])->get();
            $list['pricing_package'] = Pricing_package::where('service', $list['uuid'])->get();
            $list['faq'] = Product_faq::where('service', $list['uuid'])->get();
            $list['requirements'] = Product_requirement::where('service', $list['uuid'])->pluck('requirement');
            $list['attributes'] = Listing_attribute::where('listing_id', $list['uuid'])->get();
            $list['service_type'] = $type;
            // $list['country']
            array_push($chunk, $list);
        }
            
            array_push($chunk, $listing);
        }
        
        return response()->json([
                'status' => 'success',
                'message' => 'My event planning products retrieved successfully',
                'data' => $chunk
        ], 200);
    }

    public function deleteEventPlan($event) {
        $authUser = Auth::user();

        if(Event_planning::where('uuid', $event)->where('user_id', $authUser['uuid'])->delete()) {
            Event_checklist::where('event_id', $event)->delete();
            Event_supplier::where('event_id', $event)->delete();
            Group::where('event_id', $event)->delete();
            Group_member::where('event_id', $event)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Event plan deleted successfully'
            ], 200);
    
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Event plan can not be deleted'
            ], 200);
    
        }

        
    }

    public function removeEventSupplier($event, $listing) {
        $authUser = Auth::user();


        if(Event_supplier::where('event_id', $event)->where('listing_id', $listing)->where('user_id', $authUser['uuid'])->delete()) {

            return response()->json([
                'status' => 'success',
                'message' => 'Event supplier deleted successfully'
            ], 200);
    
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Event supplier can not be deleted'
            ], 200);
    
        }


    }

    public function addEventSupplier(Request $request, $event) {
        $authUser = Auth::user();
        $supplier = $request->all();
        $supplierVariable = array(
            "listing_id" => $supplier['id'],
            "event_supplier_features" => ['attributes' => $supplier['attributes'], 'price' => $supplier['price'], 'quantity' => $supplier['quantity']],
            "user_id" => $authUser['uuid'],
            "event_id" => $event,
            "group_id" => Group::where('event_id', $event)->pluck('uuid')->first()
        );

        if(Event_supplier::where('event_id', $event)->where('listing_id', $supplier['id'])->doesntExist()) {
            Event_supplier::create($supplierVariable);

            return response()->json([
                'status' => 'success',
                'message' => 'Supplier added successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Supplier already exists'
            ], 200);
        }

        
    }

    public function getGroupData($uuid) {
        $group = Group::where('uuid', $uuid)->first();
        $group['members'] = Group_member::where('group', $uuid)->get();

        return response()->json([
            'status' => 'success',
            'data' => $group,
            'message' => 'Group retrieved successfully'
        ], 200);
    }

    public function sendMessage(Request $request) {
        $user = Auth::user();
        $data = $request->all();

        
    }
}
