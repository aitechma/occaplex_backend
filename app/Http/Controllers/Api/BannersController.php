<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Banner;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Http\Request;

class BannersController extends Controller
{
    //

    public function get($type) {
        $banners = Banner::where('location', $type)->where('status', 1)->take(4)->get();
        return response()->json([
            'status' => 'success',
            'message' => 'Banner retrieved successfully',
            'data' => $banners
            ], 200);
    }

    public function create(Request $request) {
        $data = $request->all();
        $user = Auth::user();

        $data['owner'] = $user['uuid'];
        $data['banner_uuid'] = Str::uuid();
        $data['status'] = 0;

        if(Banner::create($data)) {
            return response()->json([
                'status' => 'success',
                'message' => 'Banner created successfully'
                ], 200);
        } else {
            return response()->json([
            'status' => 'failed',
            'message' => 'Error occured'
            ], 200);
        }
        
    }
}
