<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use App\Event_type;
use App\User;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    //
    
    public function addEventType(Request $request) {
        $admin = Auth()->user();
        
        $data = $request->all();

        $validator = Validator::make($request->all(), [ 
            'event_type' => 'required'
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);  
        }
        
        if($admin['role_id'] == 1)
        {
            if(Event_type::where('event_type', $data['event_type'])->count() > 0) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Event type already exists'
            ], 401);
        }
        
        if(Event_type::create($data)) {
             return response()->json([
                'status' => 'success',
                'message' => 'Event type created successfully',
                'event_types' => Event_type::get()
           ], 200);
        }   
        }else
        {
            return response()->json([
                'message' => 'Unauthorized User'
            ],401);
        }
        
    }
    
    public function fetchEventType() {
        $event_types = Event_type::where('parent', null)->get();
        
        $chunk = [];
        
        foreach($event_types as $category) {
            $children = Event_type::where('parent', $category['uuid'])->get();
            $category['children'] = $children;
            
            if($category['parent'] == null) {
                array_push($chunk, $category);
            }
        }
        
        return response()->json([
            'status' => 'success',
            'event_types' => $event_types
        ], 200);
        
    }
}
