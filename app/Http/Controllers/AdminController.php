<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\Vendor;
use App\Listing;

class AdminController extends Controller
{

    public function index()
    {
        return view('admin');
    }
    public function getCategories(){
        $categories = Category::all();
        return view('categories.index',compact('categories'));
    }

    public function storeCategory(Request $request)
    {
      //dd($request);
      $data = $request->all();
      $this->validate($request,[
        'name'=> 'required|max:50'
      ]);

      //$category = new Category;
      //$category->name = $data['name'];
      //$category->save();
      Category::create($data);
      return redirect()->back();
    }

    public function editCategory($uuid)
    {
        $category = Category::find($uuid);
        //dd($category);
        return view('categories.edit',compact('category'));
    }

    public function updateCategory(Request $request,$uuid)
    {

        $data = $request->all();
        $category = Category::find($uuid);

        $category->update($data);

        return redirect()->route('categories.index');
    }
    public function DeleteCategory($uuid)
    {
      $category = Category::findOrFail($uuid);
      $category->delete();

      return redirect()->route('categories.index');
    }

    public function getVendors(){
      $vendors = Vendor::all();
      return view('vendors.index',compact('vendors'));
    }
    public function updateVendor(Request $request,$uuid)
    {
      $vendor = Vendor::findOrFail($uuid);
      $data = $request->all();
      $vendor->update($data);

      return redirect()->back();
    }

    public function approveVendor($uuid)
    {
      $vendor = Vendor::where('uuid',$uuid)->first();
      $vendor->business_confirmed = 1;
      $vendor->save();

      return redirect()->back();
    }

    public function searchVendor(Request $request)
    {
      dd($request);
    }

    public function deleteVendor($uuid)
    {
      $vendor = Vendor::findOrFail($uuid);
      $vendor->delete();

      return redirect()->back();

    }

    public function getListings()
    {
      $listings = Listing::all();

      return view('listings.index',compact('listings'));
    }
    public function storeListing(Request $request)
    {
      $data = $request->all();
      $this->validate($request,[
        'title'=>'required|max:255'
      ]);
      Listing::create($data);
      return redirect()->back();
    }
    public function updateListing(Request $request,$uuid)
    {
      $data = $request->all();
      $listing = Listing::findOrFail($uuid);
      $listing->update($data);
      return redirect()->back();
    }
    public function deleteListing($uuid)
    { 
      $listing  = Listing::findOrFail($uuid);
      $listing->delete();

      return redirect()->back();
    }
    public function getCustomers()
    {
      $customers = User::all();
      return view('customers.index',compact('customers'));
    }
    public function updateCustomer(Request $request,$uuid)
    {
      $data = $request->all();
      $customer = User::findOrFail($uuid);

      $customer->update($data);
      return redirect()->back();
    }
}
