<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing_attribute extends Model
{
    //
    protected $fillable = ['listing_id',	'name',	'values',	'vendor_id',	'status'];
    
//     protected $casts = [
//         'values' => 'array'
        
//     ];
}
