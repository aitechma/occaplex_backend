<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $fillable = [
        'url', 'content_type', 'user','media_type','size','storage_id','width','height','alt','height'
        ];
}
