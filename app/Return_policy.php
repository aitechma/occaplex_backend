<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Return_policy extends Model
{
    //
    
    protected $fillable = ['user',	'vendor',	'return_policy'	,'status'];

    protected $casts = [
		'updated_at' => 'datetime'
	];
}
