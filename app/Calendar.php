<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $fillable = [
    	'unavailable_date','note','vendor'
    ];
}
