<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Banner extends Model
{
    //

    // protected $primaryKey = 'uuid';

    protected $fillable = ['owner', 'url', 'location', 'views', 'clicks', 'status', 'link', 'ad_size', 'banner_uuid'];
}
