<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum_answer extends Model
{
    //
    protected $fillable = ["forum", "answer", "user", "response_to", "deleted"];
}
