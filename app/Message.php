<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable = ['sender',	'recipient',	'read',	'status',	'media',	'message',	'media_type'	];
}
