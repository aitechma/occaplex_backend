<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Category extends Model
{
    use HasSlug;
    //
    protected $primaryKey = 'uuid';
    
    protected $fillable = [
        'name', 'image', 'color', 'excerpt', 'parent'    
    ];
    
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')
            ->usingSeparator('-')
            ->slugsShouldBeNoLongerThan(50);
            //->doNotGenerateSlugsOnUpdate();
    }
}
