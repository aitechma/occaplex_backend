<?php

use Illuminate\Http\Request;

use Igaster\LaravelCities\Geo;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Version 1 



Route::group(['prefix'=> 'v1'], function () {
    
    \Igaster\LaravelCities\Geo::ApiRoutes();

    Route::get('ip', function () {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $data = Location::get($ip);
        dd(\Request::ip());
       
    });

    Route::get('countries', function() {
       return Geo::getCountries();
    });

    Route::post('register', 'Api\UsersController@register');
    Route::post('verify/email', 'Api\UsersController@verifyEmail');
    Route::post('verify/email/resend', 'Api\UsersController@resendVerifyEmail');
    Route::post('login', 'Api\UsersController@login');
    Route::post('social_auth/{provider}', 'Api\UsersController@socialAuth');
    Route::post('password_reset/send_link', 'Api\UsersController@passwordReset');
    Route::post('password_reset/validate', 'Api\UsersController@passwordResetValidate');
    
    Route::get('service/all', 'Api\ServiceController@getListing');
    Route::get('service/deals', 'Api\ServiceController@getDealListing');
    Route::get('categories', 'Api\ServiceController@fetchCategory');
    Route::get('event_types', 'Api\EventController@fetchEventType');
    Route::get('rental_options', 'Api\ServiceController@fetchRentalOption');
    Route::get('service/{slug}', 'Api\ServiceController@getListingBySlug');
    Route::get('listing/{uuid}', 'Api\ServiceController@getListingById');
    Route::get('listing/vendor/username/{username}','Api\ServiceController@getVendorListingsbyUsername');
    Route::get('listing/vendor/id/{uuid}','Api\ServiceController@getVendorListingsbyId');
    Route::get('banner/{type}', 'Api\BannersController@get');

    Route::post('service/many', 'Api\ServiceController@getManyListingsById');
    
    //Administrative
    
    Route::post('create/rental_option', 'Api\ServiceController@addRentalOption');
    
    Route::post('create/banner', 'Api\BannersController@create');

    Route::get('pages/{name}', 'Api\StaticPagesController@getPages');

    //Vendor view page
    Route::get('vendor/view/business_details/{id}', 'Api\VendorsController@viewBusinessDetails');
    Route::get('vendor/view/vendor_listing/{vendor}', 'Api\ServiceController@vendorListing');

    Route::group(['middleware' => 'auth:api'], function () {
        //all authenticates here
        Route::get('profile', 'Api\UsersController@MyProfile');
        Route::post('change/password','Api\UsersController@changePassword');
        
        Route::post('mailtest','Api\UsersController@mailTest');


        Route::put('profile/update', 'Api\UsersController@UpdateProfile');
        Route::put('profile/picture_upload', 'Api\UsersController@UpdateProfilePicture');
        Route::put('user/update/accounttype/{uuid}','Api\UsersController@updateAccountType');
        Route::get('services/mine', 'Api\ServiceController@myListing');
        
        Route::post('service/add/{status}', 'Api\ServiceController@addListing');
        Route::post('create/category', 'Api\ServiceController@addCategory');

        Route::post('create/order/{gateway}', 'Api\OrdersController@placeOrder');
        //Route::get('test/order/{orderUuid}', 'Api\OrdersController@testOrder');
        Route::get('orders/{account}', 'Api\OrdersController@getOrders');
        Route::post('orders/add/note','Api\OrdersController@addNotes');
        Route::get('orders/get/note/{order}','Api\OrdersController@getNotes');
        Route::put('orders/update/status','Api\OrdersController@updateOrderStatus');
        Route::delete('orders/cancel/order/{order}','Api\OrdersController@cancelOrder');
        Route::post('orders/check/vendor/date', 'Api\OrdersController@checkDate');
        Route::get('orders/uuid/{uuid}','Api\OrdersController@getOrderByUuid');


        Route::post('create/event_type', 'Api\EventController@addEventType');



        // Wishlist
        
        Route::post('wishlist/create', 'Api\WishlistController@save');
        Route::get('wishlists', 'Api\WishlistController@get');
        Route::delete('wishlist/delete/{id}', 'Api\WishlistController@remove');
        
        // Event planning

        Route::get('my/groups', 'Api\EventPlanningController@getGroups');
        Route::post('create/group', 'Api\EventPlanningController@createGroup');

        Route::post('create/event', 'Api\EventPlanningController@createEvent');

        Route::get('get/event/{uuid}', 'Api\EventPlanningController@getEventById');
        Route::put('update/event/{uuid}', 'Api\EventPlanningController@updateEventById');
        
        Route::post('event/planning', 'Api\EventPlanningController@submit');
        Route::get('event/my_planning', 'Api\EventPlanningController@myEventPlanning');
        Route::get('event/my_planning/products/{id}', 'Api\EventPlanningController@myEventPlanningProducts');
        Route::delete('event/planning/delete/{id}', 'Api\EventPlanningController@deleteEventPlan');

        Route::delete('event/supplier/remove/{event}/{listing}', 'Api\EventPlanningController@removeEventSupplier');

        Route::post('event/supplier/add/{event}', 'Api\EventPlanningController@addEventSupplier');
        
        // Generate checklist
        
        Route::post('event/checklist', 'Api\EventPlanningController@checklist');
        Route::post('event/checklist/vendors', 'Api\EventPlanningController@recommendVendors');
        
        //Vendor Account
        
        Route::post('vendor/application', 'Api\VendorsController@becomeVendor');
        Route::get('vendor/business_details', 'Api\VendorsController@businessDetails');
        Route::put('vendor/update_business_details', 'Api\VendorsController@updateBusinessDetails');
        Route::put('vendor/update_business_logo', 'Api\VendorsController@updateBusinessLogo');

        Route::put('vendor/update_business_doc/{type}', 'Api\VendorsController@updateBusinessDoc');

        Route::post('vendor/update_cancel_policy', 'Api\VendorsController@updateCancelPolicy');
        Route::get('vendor/get_cancel_policy', 'Api\VendorsController@getCancelPolicy');
        Route::post('vendor/create_deal', 'Api\ServiceController@createDeal');
        Route::post('vendor/delete_deal', 'Api\ServiceController@deleteDeal');
        Route::get('vendor/deals', 'Api\ServiceController@getDeals');

        Route::delete('listing/delete/{uuid}', 'Api\ServiceController@deleteListing');
        
        //User view page
        
        Route::post('profile/others', 'Api\UsersController@UserProfile');
        Route::get('vendor/user/{id}', 'Api\UsersController@userVendor');
        Route::post('friend_request/send/{friend}', 'Api\UsersController@sendFriendRequest');
        Route::post('message/send', 'Api\MessageController@sendMessage');
        Route::post('message/read/{message}', 'Api\MessageController@readMessage');
        
        
        
        
        
        //Customer account
        
        Route::post('friend_request/accept/{request}', 'Api\UsersController@acceptFriendRequest');
        Route::post('friend_request/reject/{request}', 'Api\UsersController@rejectFriendRequest');
        Route::post('friend_request/remove/{friend}', 'Api\UsersController@removeFriend');
        Route::get('friend_request/mine', 'Api\UsersController@myRequests');
        Route::get('friend_request/friends/{user}', 'Api\UsersController@userFriends');
        Route::get('contacts', 'Api\MessageController@myContacts');
        Route::get('messages/{contact}', 'Api\MessageController@myMessages');

        // Forum

        Route::post('create-topic', 'Api\ForumController@createTopic');
        Route::get('forum-topics/{type}', 'Api\ForumController@getTopics');
        Route::post('answer-question', 'Api\ForumController@answerQuestion');
        Route::get('forum-answers/{type}', 'Api\ForumController@getTopicAnswers');
        Route::get('answer/{type}/{forum}/{answer}', 'Api\ForumController@voteAnswer');
        Route::get('forum/{id}', 'Api\ForumController@getTopicDetails');

        // Payment methods

        Route::post('add-payment-method', 'Api\PaymentMethodController@updatePaymentMethod');
        Route::get('get-payment-method', 'Api\PaymentMethodController@getPayMethod');


        // Get my groups
        // Route::get('get-payment-method', 'Api\PaymentMethodController@getPayMethod');

        //Unavailable dates
        Route::post('vendor/addunavailabledate','Api\VendorsController@addUnavailableDate');
        Route::delete('vendor/delete/unavailabledate/{uuid}','Api\VendorsController@deleteUnavailableDate');
        Route::put('vendor/update/unavailabledate/{uuid}','Api\VendorsController@editUnavailableDate');
        Route::get('vendor/get/unavailabledates','Api\VendorsController@getUnavailableDates');
        Route::post('vendor/check/unavailabledates','Api\VendorsController@checkUnavailableDate');
        Route::post('vendor/analytics','Api\VendorsController@vendorAnalytics');
        /**
         * ADMINISTRATOR ROUTES STARTS HERE
         */
        Route::get('admin/get/users', 'Api\AdminController@getUsers');
        Route::get('admin/get/users/{uuid}','Api\AdminController@getUser');
        Route::get('admin/get/vendors', 'Api\AdminController@getVendors');
        Route::get('admin/get/vendors/{uuid}','Api\AdminController@getVendor');
        Route::get('admin/get/listings','Api\AdminController@getListings');
        Route::get('admin/get/listings/{uuid}','Api\AdminController@getListing');
        Route::get('admin/get/categories','Api\AdminController@getCategories');
        Route::get('admin/get/categories/{uuid}','Api\AdminController@getCategory');
        Route::put('admin/edit/category/{uuid}','Api\AdminController@editCategory');
        Route::get('admin/get/eventtypes','Api\AdminController@getEventTypes');
        Route::get('admin/get/eventtypes/{uuid}','Api\AdminController@getEventType');
        Route::delete('admin/delete/category/{uuid}', 'Api\AdminController@deleteCategory');
        Route::put('admin/update/category/{uuid}', 'Api\AdminController@updateCategory');
        Route::delete('admin/delete/eventtype/{uuid}','Api\AdminController@deleteEventType');
        Route::put('admin/edit/eventtype/{uuid}','Api\AdminController@editEventType'); 
        Route::put('admin/approve/vendor/{uuid}','Api\AdminController@approveVendorBusiness');
        Route::put('admin/deny/vendor/{uuid}','Api\AdminController@denyVendorBusiness');
        Route::put('admin/suspend/vendor/{uuid}','Api\AdminController@suspendVendorBusiness');
        Route::put('admin/unapprove/vendor/{uuid}','Api\AdminController@unapproveVendorBusiness');
        Route::put('admin/pending/listing/{uuid}','Api\AdminController@updateListingStatusToPending');
        Route::put('admin/approve/listing/{uuid}','Api\AdminController@updateListingStatusToApprove');
        Route::put('admin/unapprove/listing/{uuid}','Api\AdminController@updateListingStatusToUnapprove');
        Route::put('admin/suspend/listing/{uuid}','Api\AdminController@updateListingStatusToSuspend');

        Route::get('admin/orders/listing/{uuid}','Api\AdminController@getOrdersByListing');
        Route::get('admin/listings/vendor/{uuid}','Api\AdminController@getListingsByVendor');

        Route::post('admin/vendor/analytics/{uuid}','Api\AdminController@vendorAnalytics');
        Route::post('admin/analytics','Api\AdminController@adminAnalytics');

        Route::post('generate/template','Api\UsersController@generateTemplate');
        //Route::get('templates','Api\UsersController@getTemplates');
        //Route::post('template/content/{templateId}','Api\UsersController@verifyEmailTemplate');
        Route::post('send/mail','Api\UsersController@sendMail');


        //Review
        Route::post('add/review','Api\ReviewController@addReview');
        Route::get('review/service/{service}','Api\ReviewController@getReviewByService');
        Route::get('vendor/review/{vendorUuid}','Api\ReviewController@getVendorsReview');
        Route::get('user/reviews','Api\ReviewController@getMyReviews');
    });
    
});


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
