<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    //echo('This is a restricted area');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('categories/index','AdminController@getCategories')->name('categories.index');
Route::post('categories/create','AdminController@storeCategory')->name('categories.create');
Route::get('/categories/{category}/edit','AdminController@editCategory')->name('categories.edit');

Route::put('/categories/{category}','AdminController@updateCategory')->name('categories.update');
Route::delete('/categories/{category}','AdminController@deleteCategory');

Route::get('vendors/index','AdminController@getVendors')->name('vendors.index');
Route::put('vendors/{vendor}','AdminController@updateVendor')->name('vendors.update');
Route::put('vendors/approve/{vendor}','AdminController@approveVendor')->name('vendors.approve');
Route::post('vendors/search','AdminController@searchVendor')->name('vendors.search');
Route::delete('vendors/{vendor}','AdminController@deleteVendor')->name('vendors.delete');

Route::get('listings/index','AdminController@getListings')->name('listings.index');
Route::post('listings/create','AdminController@storeListing')->name('listings.create');
Route::put('listings/{listing}','AdminController@updateListing')->name('listings.update');
Route::delete('listings/{listing}','AdminController@deleteListing')->name('listings.delete');

Route::get('customers/index','AdminController@getCustomers')->name('customers.index');
Route::put('customers/{customer}','AdminController@updateCustomer')->name('customers.update');
//Route::put('customers/{customer}','AdminController@updateCustomer')->name('customers.update');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin');
