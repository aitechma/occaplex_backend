<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventInviteLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_invite_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event', 191)->nullable()->default(null);
            $table->longText('link')->nullable()->default(null);
            $table->boolean('validity')->nullable()->default(1);
            $table->string('user', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_invite_links');
    }
}
