<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 190)->nullable()->default(null);
            $table->mediumText('slug')->nullable()->default(null);
            $table->mediumText('header')->nullable()->default(null);
            $table->mediumText('sub_header')->nullable()->default(null);
            $table->mediumText('excerpt')->nullable()->default(null);
            $table->longText('section1_content')->nullable()->default(null);
            $table->mediumText('section1_image')->nullable()->default(null);
            $table->longText('section2_content')->nullable()->default(null);
            $table->mediumText('section2_image')->nullable()->default(null);
            $table->longText('section3_content')->nullable()->default(null);
            $table->mediumText('section3_image')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_pages');
    }
}
