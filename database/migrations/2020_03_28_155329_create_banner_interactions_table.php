<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_interactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('banner_uuid', 150)->nullable()->default(null);
            $table->string('interaction_type', 150)->nullable()->default(null);
            $table->string('user_ip', 150)->nullable()->default(null);
            $table->string('country', 150)->nullable()->default(null);
            $table->string('region', 150)->nullable()->default(null);
            $table->string('city', 150)->nullable()->default(null);
            $table->string('latitude', 150)->nullable()->default(null);
            $table->string('longitude', 150)->nullable()->default(null);
            $table->string('zipcode', 150)->nullable()->default(null);
            $table->timestamps();
        });

        Schema::table('banners', function (Blueprint $table) {
            //
            $table->string('banner_uuid', 150)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_interactions');
    }
}
