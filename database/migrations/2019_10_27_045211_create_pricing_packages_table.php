<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable()->default(null);
            $table->string('day_needed', 100)->nullable()->default(null);
            $table->string('faster_amount', 100)->nullable()->default(null);
            $table->string('flavours', 100)->nullable()->default(null);
            $table->string('title', 100)->nullable()->default(null);
            $table->string('service', 100)->nullable()->default(null);
            $table->string('vendor', 100)->nullable()->default(null);
            $table->string('status', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_packages');
    }
}
