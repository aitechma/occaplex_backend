<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable()->default(null);
            $table->mediumText('title')->nullable()->default(null);
            $table->longText('category')->nullable()->default(null);
            $table->longText('service_type')->nullable()->default(null);
            $table->mediumText('tags')->nullable()->default(null);
            $table->string('dimension', 100)->nullable()->default(null);
            $table->string('pricing_package', 100)->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->longText('requirements')->nullable()->default(null);
            $table->string('gallery', 100)->nullable()->default(null);
            $table->string('total_price', 100)->nullable()->default(null);
            $table->string('country', 100)->nullable()->default(null);
            $table->string('city', 100)->nullable()->default(null);
            $table->string('region', 100)->nullable()->default(null);
            $table->longText('slug')->nullable()->default(null);
            $table->boolean('verification')->nullable()->default(false);
            $table->boolean('status')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
