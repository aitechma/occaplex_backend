<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventPlanningGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_planning_group', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event_id', 100)->nullable()->default(null);
            $table->string('user_id', 100)->nullable()->default(null);
            $table->string('listing_id', 100)->nullable()->default(null);
            $table->string('vendor_id', 100)->nullable()->default(null);
            $table->longText('member_id')->nullable()->default(null);
            $table->string('status', 100)->nullable()->default(null);
            $table->string('approval', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_planning_group');
    }
}
