<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('uuid')->nullable()->default(null);
            $table->bigIncrements('id');
            $table->string('username', 100)->nullable()->default(null)->unique();
            $table->string('business_name', 190)->nullable()->default(null)->unique();
            $table->string('phone', 100)->nullable()->default(null);
            $table->string('country', 100)->nullable()->default(null);
            $table->string('country_code', 100)->nullable()->default(null);
            $table->string('city', 100)->nullable()->default(null);
            $table->string('zipcode', 100)->nullable()->default(null);
            $table->longText('address')->nullable()->default(null);
            $table->string('name')->default(null);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('bill_email')->nullable()->default(null);
            $table->string('bill_phone', 100)->nullable()->default(null);
            $table->string('bill_country', 100)->nullable()->default(null);
            $table->string('bill_country_code', 100)->nullable()->default(null);
            $table->string('bill_city', 100)->nullable()->default(null);
            $table->string('bill_zipcode', 100)->nullable()->default(null);
            $table->longText('bill_address')->nullable()->default(null);
            $table->longText('about_me')->nullable()->default(null);
            $table->longText('about_service')->nullable()->default(null);
            $table->string('account_type', 100)->nullable()->default('customer');
            $table->boolean('confirmed')->nullable()->default(false);
            $table->boolean('business_confirmed')->nullable()->default(false);
            $table->longText('picture')->nullable()->default(null);
            $table->boolean('online')->nullable()->default(false);
            $table->boolean('phone_verified')->nullable()->default(false);
            $table->boolean('email_verified')->nullable()->default(false);
            $table->boolean('location_verified')->nullable()->default(false);
            $table->string('facebook', 150)->nullable()->default(null);
            $table->boolean('facebook_verified')->nullable()->default(false);
            $table->string('twitter', 150)->nullable()->default(null);
            $table->boolean('twitter_verified')->nullable()->default(false);
            $table->string('youtube', 150)->nullable()->default(null);
            $table->boolean('youtube_verified')->nullable()->default(false);
            $table->string('instagram', 150)->nullable()->default(null);
            $table->boolean('instagram_verified')->nullable()->default(false);
            $table->string('website', 150)->nullable()->default(null);
            $table->boolean('website_verified')->nullable()->default(false);
            $table->longText('service_category')->nullable()->default(null);
            $table->longText('interest_category')->nullable()->default(null);
            $table->longText('languages')->nullable()->default(null);
            $table->string('primary_language', 150)->nullable()->default(null);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
