<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->uuid('uuid')->nullable()->default(null);
            $table->bigIncrements('id');
            $table->string('username', 100)->unique();
            $table->string('business_name', 190)->nullable()->default(null)->unique();
            $table->string('business_phone', 100)->nullable()->default(null);
            $table->string('alt_business_phone', 100)->nullable()->default(null);
            $table->string('country', 100)->nullable()->default(null);
            $table->string('city', 100)->nullable()->default(null);
            $table->string('region', 100)->nullable()->default(null);
            $table->string('zipcode', 100)->nullable()->default(null);
            $table->longText('address')->nullable()->default(null);
            $table->string('user_id', 100)->default(null);
            $table->string('business_email',100)->unique();
            $table->string('email_verified_at',100)->nullable()->default(null);
            $table->string('phone_verified_at',100)->nullable()->default(null);
            $table->longText('about_business')->nullable()->default(null);
            $table->boolean('business_confirmed')->nullable()->default(false);
            $table->longText('business_logo')->nullable()->default(null);
            $table->boolean('online')->nullable()->default(false);
            $table->boolean('phone_verified')->nullable()->default(false);
            $table->boolean('email_verified')->nullable()->default(false);
            $table->boolean('location_verified')->nullable()->default(false);
            $table->string('facebook', 150)->nullable()->default(null);
            $table->boolean('facebook_verified')->nullable()->default(false);
            $table->string('twitter', 150)->nullable()->default(null);
            $table->boolean('twitter_verified')->nullable()->default(false);
            $table->string('youtube', 150)->nullable()->default(null);
            $table->boolean('youtube_verified')->nullable()->default(false);
            $table->string('instagram', 150)->nullable()->default(null);
            $table->boolean('instagram_verified')->nullable()->default(false);
            $table->string('website', 150)->nullable()->default(null);
            $table->boolean('website_verified')->nullable()->default(false);
            $table->longText('service_category')->nullable()->default(null);
            $table->longText('languages')->nullable()->default(null);
            $table->string('primary_language', 150)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
