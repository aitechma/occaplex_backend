<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_votes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('forum', 191)->nullable()->default(null);
            $table->string('answer', 191)->nullable()->default(null);
            $table->string('user', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_votes');
    }
}
