<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToEventPlanning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_planning', function (Blueprint $table) {
            //
            $table->string('status', 190)->nullable()->default(NULL);
            $table->string('total_price', 190)->nullable()->default(NULL);
            $table->string('paid', 190)->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_planning', function (Blueprint $table) {
            //
        });
    }
}
