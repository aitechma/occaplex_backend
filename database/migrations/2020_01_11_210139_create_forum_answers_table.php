<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('forum', 191)->nullable()->default(null);
            $table->string('user', 191)->nullable()->default(null);
            $table->longText('answer')->nullable()->default(null);
            $table->string('response_to', 191)->nullable()->default(null);
            $table->boolean('deleted')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_answers');
    }
}
