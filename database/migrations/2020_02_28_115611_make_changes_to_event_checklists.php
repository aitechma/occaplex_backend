<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeChangesToEventChecklists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_checklists', function (Blueprint $table) {
            //
            $table->renameColumn('listing_id', 'category_id');
            $table->renameColumn('list_price', 'budget');
            $table->string('group_id', 190)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_checklists', function (Blueprint $table) {
            //
        });
    }
}
