<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleryMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gallery', 191)->nullable()->default(null);
            $table->longText('media_url', 191)->nullable()->default(null);
            $table->string('media_type', 191)->nullable()->default(null);
            $table->string('status', 191)->nullable()->default(null);
            $table->string('media_event', 191)->nullable()->default(null);
            $table->string('media_vendor', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_metas');
    }
}
