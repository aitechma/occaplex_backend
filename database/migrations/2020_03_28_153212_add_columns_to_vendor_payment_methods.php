<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToVendorPaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_payment_methods', function (Blueprint $table) {
            //
            $table->string('paypal_id', 150)->nullable()->default(null);
            $table->string('stripe_id', 150)->nullable()->default(null);
            $table->string('bank_name', 150)->nullable()->default(null);
            $table->string('bank_number', 150)->nullable()->default(null);
            $table->string('bank_sort_code', 150)->nullable()->default(null);
            $table->string('account_name', 150)->nullable()->default(null);
            $table->string('bank_verification_number', 150)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_payment_methods', function (Blueprint $table) {
            //
        });
    }
}
