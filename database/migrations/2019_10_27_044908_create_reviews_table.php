<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable()->default(null);
            $table->string('content', 100)->nullable()->default(null);
            $table->string('rating', 100)->nullable()->default(null);
            $table->string('service', 100)->nullable()->default(null);
            $table->string('vendor', 100)->nullable()->default(null);
            $table->string('communication_level', 100)->nullable()->default(null);
            $table->string('deliver_on_time', 100)->nullable()->default(null);
            $table->string('recommend_to_friend', 100)->nullable()->default(null);
            $table->string('status', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
