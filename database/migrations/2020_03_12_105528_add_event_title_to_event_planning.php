<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEventTitleToEventPlanning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_planning', function (Blueprint $table) {
            //
            $table->longText('event_title')->nullable()->default(null);
        });

        Schema::table('event_suppliers', function (Blueprint $table) {
            //
            $table->longText('event_supplier_features')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_planning', function (Blueprint $table) {
            //
        });
    }
}
