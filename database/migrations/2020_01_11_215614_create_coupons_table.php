<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vendor', 191)->nullable()->default(null);
            $table->string('user', 191)->nullable()->default(null);
            $table->string('product', 191)->nullable()->default(null);
            $table->string('code', 191)->nullable()->default(null);
            $table->string('expiration', 191)->nullable()->default(null);
            $table->string('limit', 191)->nullable()->default(null);
            $table->string('usage', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
