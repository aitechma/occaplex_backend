<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyColumnsToListings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            //
            $table->string('width', 190)->nullable()->default(null);
            $table->string('height', 190)->nullable()->default(null);
            $table->string('weight', 190)->nullable()->default(null);
            $table->string('length', 190)->nullable()->default(null);
            $table->boolean('featured')->nullable()->default(null);
            $table->renameColumn('shipping', 'shipping_class');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listings', function (Blueprint $table) {
            //
        });
    }
}
