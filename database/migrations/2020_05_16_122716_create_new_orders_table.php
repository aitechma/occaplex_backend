<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable()->default(null);
            $table->string('ref', 100)->nullable()->default(null);
            $table->string('gref', 100)->nullable()->default(null);
            $table->longText('product')->nullable()->default(null);
            $table->string('quantity', 100)->nullable()->default(null);
            $table->string('listing_type', 100)->nullable()->default(null);
            $table->string('plan', 100)->nullable()->default(null);
            $table->string('sales_price', 100)->nullable()->default(null);
            $table->string('checkout_fee', 100)->nullable()->default(null);
            $table->string('tax', 100)->nullable()->default(null);
            $table->string('shipping_fee', 100)->nullable()->default(null);
            $table->string('total_price', 100)->nullable()->default(null);
            $table->string('vendor_revenue', 100)->nullable()->default(null);
            $table->string('vendor_charge', 100)->nullable()->default(null);
            $table->string('vendor_uuid', 100)->nullable()->default(null);
            $table->string('user_uuid', 100)->nullable()->default(null);
            $table->string('product_uuid', 100)->nullable()->default(null);
            $table->string('order_status', 100)->nullable()->default(null);
            $table->string('payment_status', 100)->nullable()->default(null);
            $table->string('payment_method', 100)->nullable()->default(null);
            $table->string('payment_ref_id', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_orders');
    }
}
