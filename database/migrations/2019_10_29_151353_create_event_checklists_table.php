<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_checklists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event_id', 100)->nullable()->default(null);
            $table->string('user_id', 100)->nullable()->default(null);
            $table->string('listing_id', 100)->nullable()->default(null);
            $table->string('vendor_id', 100)->nullable()->default(null);
            $table->string('list_price', 100)->nullable()->default(null);
            $table->string('status', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_checklists');
    }
}
