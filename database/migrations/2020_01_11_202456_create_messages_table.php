<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sender', 191)->nullable()->default(null);
            $table->string('recipient', 191)->nullable()->default(null);
            $table->boolean('read')->nullable()->default(0);
            $table->string('status', 191)->nullable()->default(null);
            $table->longText('media')->nullable()->default(null);
            $table->longText('message')->nullable()->default(null);
            $table->string('media_type', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
