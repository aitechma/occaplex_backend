<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->mediumText('title')->nullable()->default(null);
            $table->longText('article')->nullable()->default(null);
            $table->string('slug')->nullable()->default(null);
            $table->string('author', 192)->nullable()->default('occaplex');
            $table->string('category', 192)->nullable()->default(null);
            $table->boolean('published')->nullable()->default(0);
            $table->string('status', 192)->nullable()->default(null);
            $table->longText('image')->nullable()->default(null);
            $table->boolean('trendy')->nullable()->default(0);
            $table->boolean('trendy_status')->nullable()->default(0);
            $table->string('read', 192)->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
    }
}
