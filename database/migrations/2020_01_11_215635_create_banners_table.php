<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('owner', 191)->nullable()->default(null);
            $table->longText('url', 191)->nullable()->default(null);
            $table->string('location', 191)->nullable()->default(null);
            $table->string('views', 191)->nullable()->default(null);
            $table->string('clicks', 191)->nullable()->default(null);
            $table->string('status', 191)->nullable()->default(null);
            $table->longText('link', 191)->nullable()->default(null);
            $table->string('ad_size', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
