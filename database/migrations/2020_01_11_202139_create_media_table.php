<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('url')->nullable()->default(null);
            $table->string('content_type', 191)->nullable()->default(null);
            $table->string('user', 191)->nullable()->default(null);
            $table->string('media_type', 191)->nullable()->default(null);
            $table->string('size', 191)->nullable()->default(null);
            $table->string('storage_id', 191)->nullable()->default(null);
            $table->string('alt', 191)->nullable()->default(null);
            $table->string('width', 191)->nullable()->default(null);
            $table->string('height', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
