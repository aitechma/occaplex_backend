<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user', 191)->nullable()->default(null);
            $table->mediumText('title')->nullable()->default(null);
            $table->longText('media')->nullable()->default(null);
            $table->string('media_type', 191)->nullable()->default(null);
            $table->string('status', 191)->nullable()->default(null);
            $table->string('group', 191)->nullable()->default(null);
            $table->string('reply_to', 191)->nullable()->default(null);
            $table->boolean('approved')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_posts');
    }
}
