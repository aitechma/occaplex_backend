<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('listing_id', 191)->nullable()->default(null);
            $table->string('name', 191)->nullable()->default(null);
            $table->longText('values')->nullable()->default(null);
            $table->string('vendor_id', 191)->nullable()->default(null);
            $table->string('status', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_attributes');
    }
}
