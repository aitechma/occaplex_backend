<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleryCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user', 191)->nullable()->default(null);
            $table->mediumText('comment')->nullable()->default(null);
            $table->string('status', 191)->nullable()->default(null);
            $table->string('gallery', 191)->nullable()->default(null);
            $table->string('reply_to', 191)->nullable()->default(null);
            $table->boolean('approved')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_comments');
    }
}
