<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreColumnToListings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            //
            $table->string('sku', 190)->nullable()->default(NULL);
            $table->string('quantity_per_unit', 190)->nullable()->default(NULL);
            $table->string('unit_price', 190)->nullable()->default(NULL);
            $table->string('size', 190)->nullable()->default(NULL);
            $table->string('color', 190)->nullable()->default(NULL);
            $table->string('discount', 190)->nullable()->default(0);
            $table->string('stock', 190)->nullable()->default(NULL);
            $table->string('available', 190)->nullable()->default(1);
            $table->longText('note', 190)->nullable()->default(NULL);
            $table->json('service_type')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listings', function (Blueprint $table) {
            //
        });
    }
}
