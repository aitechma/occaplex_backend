<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable()->default(NULL);
            $table->mediumText('title')->nullable()->default(NULL);
            $table->mediumText('location')->nullable()->default(NULL);
            $table->longText('description')->nullable()->default(NULL);
            $table->longText('image')->nullable()->default(NULL);
            $table->string('status', 190)->nullable()->default(NULL);
            $table->string('price', 190)->nullable()->default(NULL);
            $table->string('capacity', 190)->nullable()->default(NULL);
            $table->string('date', 190)->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
