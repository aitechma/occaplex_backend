<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event_id', 190)->nullable()->default(null);
            $table->string('user_id', 190)->nullable()->default(null);
            $table->string('group_id', 190)->nullable()->default(null);
            $table->string('listing_id', 190)->nullable()->default(null);
            $table->string('listing_purchased', 190)->nullable()->default(null);
            $table->string('listing_purchased_by', 190)->nullable()->default(null);
            $table->string('status', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_suppliers');
    }
}
