<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order', 191)->nullable()->default(null);
            $table->string('product', 191)->nullable()->default(null);
            $table->string('cost', 191)->nullable()->default(null);
            $table->string('shipping', 191)->nullable()->default(null);
            $table->string('status', 191)->nullable()->default(null);
            $table->string('delivery', 191)->nullable()->default(null);
            $table->longText('shipping_address')->nullable()->default(null);
            $table->longText('order_note')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_metas');
    }
}
