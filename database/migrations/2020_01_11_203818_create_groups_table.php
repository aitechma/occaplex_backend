<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('creator', 191)->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->longText('cover_photo')->nullable()->default(null);
            $table->mediumtext('title')->nullable()->default(null);
            $table->longText('slug')->nullable()->default(null);
            $table->string('status')->nullable()->default(null);
            $table->boolean('deleted')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
