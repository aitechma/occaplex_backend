<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_likes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user', 191)->nullable()->default(null);
            $table->string('group', 191)->nullable()->default(null);
            $table->string('group_post', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_likes');
    }
}
