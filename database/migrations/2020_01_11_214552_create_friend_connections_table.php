<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFriendConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friend_connections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sender', 191)->nullable()->default(null);
            $table->string('friend', 191)->nullable()->default(null);
            $table->boolean('accepted')->nullable()->default(0);
            $table->boolean('blocked')->nullable()->default(null);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friend_connections');
    }
}
