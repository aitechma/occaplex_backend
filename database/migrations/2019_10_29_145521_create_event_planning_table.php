<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventPlanningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_planning', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable()->default(null);
            $table->string('user_id', 100)->nullable()->default(null);
            $table->string('plan_as_group', 100)->nullable()->default(null);
            $table->string('do_it', 100)->nullable()->default(null);
            $table->string('event_type', 100)->nullable()->default(null);
            $table->timestamp('date')->nullable()->default(null);
            $table->mediumText('address')->nullable()->default(null);
            $table->string('region', 100)->nullable()->default(null);
            $table->string('city', 100)->nullable()->default(null);
            $table->string('country', 100)->nullable()->default(null);
            $table->string('population', 100)->nullable()->default(null);
            $table->mediumText('event_theme')->nullable()->default(null);
            $table->string('primary_color', 100)->nullable()->default(null);
            $table->string('secondary_color', 100)->nullable()->default(null);
            $table->mediumText('event_checklist')->nullable()->default(null);
            $table->string('planning_group', 100)->nullable()->default(null);
            $table->longText('event_activities')->nullable()->default(null);
            $table->boolean('privacy')->nullable()->default(false);
            $table->mediumText('conversation_group')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_planning');
    }
}
