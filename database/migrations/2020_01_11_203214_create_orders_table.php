<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user', 191)->nullable()->default(null);
            $table->string('vendor', 191)->nullable()->default(null);
            $table->string('product', 191)->nullable()->default(null);
            $table->string('total', 191)->nullable()->default(null);
            $table->string('shipping', 191)->nullable()->default(null);
            $table->string('expected_delivery', 191)->nullable()->default(null);
            $table->longText('attributes')->nullable()->default(null);
            $table->string('payment_method', 191)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
