@extends('layouts.adminMain')

@section('content')
	<div class="col-md-12">
		<button type="button" class="btn btn-primary btn-create" data-toggle='modal' data-target='#createCategory' >Create Category</button>

		<table class="table">
			<thead class="table-active">
				<th>Name</th>
				<th>Color</th>
				<th>Excerpts</th>
				<th>Parent</th>
				<th>Slug</th>
				<th></th>
			</thead>

			<tbody>
				@if(count($categories) > 0)
					@foreach($categories as $category)
						<tr>
							<td>{{ $category->name }}</td>
							<td>{{ $category->color }}</td>
							<td>{{ $category->excerpts }}</td>
							<td>{{ $category->parent }}</td>
							<td>{{ $category->slug }}</td>
							<td>
								<div class="row">
									<div class="col">
										<button type="button" class="btn btn-primary btn-create btn-sm" data-toggle='modal' data-target='#editCategory{{ $category->uuid }}' >Edit</button>
										
									</div>
									<div class="col">
										<form method="POST" action="{{ action('AdminController@deleteCategory',$category->uuid) }}">
											<input name="_method" type="hidden" value="DELETE">
											@csrf
											<input type="submit" name="Delete" value="Delete" class="btn btn-danger btn-sm">
										</form>
									</div>
								</div>
							</td>

						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
		<div class="modal fade" id="createCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <form method="POST" action="{{ action('AdminController@storeCategory') }}">
		        	@csrf
		        	<div class="form-group">
		        		<label for="name">Name</label>
		        		<input type="text" name="name" class="form-control" >
		        	</div>

		        	<input type="submit" name="Add Category" class="btn btn-primary btn-sm">
		        </form>

		      </div>
		    </div>
		  </div>
		</div>

		@foreach($categories as $category)
			<div class="modal fade" id="editCategory{{ $category->uuid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog modal-sm" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Edit {{ $category->name }}</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        <form method="POST" action="{{ action('AdminController@updateCategory',$category->uuid) }}">
						        	<input name="_method" type="hidden" value="PUT">
						        	@csrf
						        	<div class="form-group">
						        		<label for="name">Name</label>
						        		<input type="text" name="name" class="form-control" placeholder="{{ $category->name }}">
						        	</div>

						        	<input type="submit" name="Update" class="btn btn-primary btn-sm">
						        </form>

						      </div>
						    </div>
						  </div>
						</div>
		@endforeach
		
	</div>
@endsection