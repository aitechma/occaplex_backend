@extends('layouts.adminMain')

@section('content')
	<div class="col-md-12">
		<table class="table">
		<thead class="table-active">
			<th>Username</th>
			<th>Business Name</th>
			<th>Country</th>
			<th>City</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Account Type</th>
			<th></th>
		</thead>
		<tbody>
			@foreach($customers as $customer)
				<tr>
					<td>{{ $customer->username }}</td>
					<td>{{ $customer->business_name }}</td>
					<td>{{ $customer->country }}</td>
					<td>{{ $customer->city }}</td>
					<td>{{ $customer->phone }}</td>
					<td>{{ $customer->email }}</td>
					<td>{{ ucfirst($customer->account_type) }}</td>
					<td>
						<div class="row">
							<div class="col">
								<button type="button" class="btn btn-primary btn-create btn-sm" data-toggle='modal' data-target='#editCustomer{{ $customer->uuid }}'>Edit</button>
							</div>
							<div class="col">
								<a href="#" class="btn btn-warning btn-sm">Suspend</a>
							</div>
							<div class="col">
								<a href="#" class="btn btn-info btn-sm">Approve</a>
							</div>
						</div>
					</td>

				</tr>
			@endforeach
		</tbody>
	</table>
	@foreach($customers as $customer)
				<div class="modal fade" id="editCustomer{{ $customer->uuid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Edit Customer</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
									<form action="{{ route('customers.update', $customer->id)}}" method="POST">
				        	{{-- <input name="_method" type="hidden" value="PUT"> --}}
				        	@csrf
				        	@method('PUT')
				        	<div class="form-group">
				        		<label for="city">City</label>
				        		<input type="text" name="city" class="form-control" placeholder="{{ $customer->city}}">
				        	</div>

				        	<input type="submit" name="Update" class="btn btn-primary btn-sm">
				        </form>

				      </div>
				    </div>
				  </div>
				</div>
			@endforeach
	</div>
@endsection