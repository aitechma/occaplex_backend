@extends('layouts.adminMain')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            {{-- <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div> --}}
            <table class="table">
                <thead>
                    <th><a href="#">Vendor</a></th>
                    <th><a href="#">Listing</a></th>
                    <th><a href="{{ route('categories.index') }}">Category</a></th>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
