@extends('layouts.adminMain')

@section('content')
	{{-- <div class="container"> --}}
		<div class="col-md-12">
			<button type="button" class="btn btn-primary btn-create" data-toggle='modal' data-target='#createListing' >Add Listing</button>

			<table class="table">
				<thead class="table-active">
					<th>Title</th>
					<th>Category</th>
					<th>Description</th>
					<th>Total Price</th>
					<th>Unit price</th>
					<th>Color</th>
					<th></th>
				</thead>

				<tbody>
					@if(count($listings) > 0)
						@foreach($listings as $listing)
							<tr>
								<td>{{ $listing->title }}</td>
								<td>{{ $listing->category }}</td>
								<td>{{ $listing->description }}</td>
								<td>{{ $listing->total_price }}</td>
								<td>{{ $listing->unit_price }}</td>
								<td>{{ $listing->color }}</td>
								<td>
									<div class="row">
										<div class="col">
											<button type="button" class="btn btn-primary btn-create btn-sm" data-toggle='modal' data-target='#editListing{{ $listing->uuid }}'>Edit</button>
											
										</div>
										<div class="col">
											<form method="POST" action="{{ action('AdminController@deleteListing',$listing->uuid) }}">
												<input name="_method" type="hidden" value="DELETE">
												@csrf
												<input type="submit" name="Delete" value="Delete" class="btn btn-danger btn-sm">
											</form>
										</div>
									</div>

								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
			<div class="modal fade" id="createListing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Add Listing</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <form method="POST" action="{{ action('AdminController@storeListing') }}">
			        	@csrf
			        	<div class="form-group">
			        		<label for="title">Title</label>
			        		<input type="text" name="title" class="form-control" >
			        	</div>

			        	<input type="submit" name="Add Listing" class="btn btn-primary btn-sm">
			        </form>

			      </div>
			    </div>
			  </div>
			</div>
			@foreach($listings as $listing)
				<div class="modal fade" id="editListing{{ $listing->uuid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form method="POST" action="{{ action('AdminController@updateListing',$listing->uuid) }}" >
				        	<input name="_method" type="hidden" value="PUT">
				        	@csrf
				        	<div class="form-group">
				        		<label for="title">Title</label>
				        		<input type="text" name="title" class="form-control" placeholder="{{ $listing->title }}">
				        	</div>

				        	<input type="submit" name="Update" class="btn btn-primary btn-sm">
				        </form>

				      </div>
				    </div>
				  </div>
				</div>
			@endforeach
	{{-- </div> --}}

@endsection