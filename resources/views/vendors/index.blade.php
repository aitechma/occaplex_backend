@extends('layouts.adminMain')

@section('content')
	{{-- <div class="container-fluid"> --}}
		{{-- <div class="row mt-3"> --}}
			<div class="col-md-12">
				<div class="row justify-content-end">
					<div class="col-sm-3">
							<form action="{{ action('AdminController@searchVendor') }}" method="POST">
								@csrf
								{{-- <div class="form-group"> --}}
									<div class="input-group mb-3">
										<input type="text" name="search" class="form-control" placeholder="Search Vendor">
										<div class="input-group-append">
											<button class="btn btn-outline-secondary" type="submit">Search</button>
										</div>
									</div>
								{{-- </div> --}}
							</form>
					</div>
				</div>
				{{-- <div> --}}
			<table class="table">
				<thead class="table-active">
					<th>Business Name</th>
					{{-- <th>Business Confirmed</th> --}}
					<th>Description</th>
					<th>Service Category</th>
					<th>Business Email</th>
					<th>Country</th>
					<th>City</th>
					<th>Website</th>
					<th>Phone</th>
					<th>Facebook</th>
					<th></th>
				</thead>
				<tbody>
					@if(count($vendors) > 0)
						@foreach($vendors as $vendor)
							<tr>
								<td>{{ $vendor->business_name }}</td>
								{{-- <td>{{ $vendor->business_confirmed }}</td> --}}
								<td>{{ $vendor->about_service }}</td>
								<td>{{ $vendor->service_category }}</td>
								<td>{{ $vendor->business_email }}</td>
								<td>{{ $vendor->country }}</td>
								<td>{{ $vendor->city }}</td>
								<td>{{ $vendor->website }}</td>
								<td>{{ $vendor->phone }}</td>
								<td>{{ $vendor->facebook }}</td>
								<td>
									<div class="row">
										<div class="col">
											{{-- <a href="#" class="btn btn-primary btn-sm">Edit</a> --}}
											<button type="button" class="btn btn-primary btn-create btn-sm" data-toggle='modal' data-target='#editVendor{{ $vendor->uuid }}' >Edit</button>
										</div>
										<div class="col">
											<form action="{{ action('AdminController@deleteVendor',$vendor->uuid) }}" method="POST">
												<input type="hidden" name="_method" value="DELETE">
												@csrf
												<input type="submit" name="delete" value="DELETE" class="btn btn-danger btn-sm">
											</form>
										</div>
										@if($vendor['business_confirmed'] !== 1)
											<div class="col">
												<form action="{{ action('AdminController@approveVendor',$vendor->uuid) }} " method="POST">
													<input name="_method" type="hidden" value="PUT">
													@csrf
													<input type="submit" name="business_confirmed" value="Approve" class="btn btn-primary btn-sm">
												</form>
											</div>
										@endif
									</div>
								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
			{{-- </div> --}}
		</div>
		</div>
		@foreach($vendors as $vendor)
			<div class="modal fade" id="editVendor{{ $vendor->uuid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Edit Vendor Details</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <form method="POST" action="{{ action('AdminController@updateVendor',$vendor->uuid) }}">
			        	<input name="_method" type="hidden" value="PUT">
			        	@csrf
			        	<div class="form-group">
			        		<label for="country">Country</label>
			        		<input type="text" name="country" class="form-control" >
			        	</div>

			        	<input type="submit" name="Update" class="btn btn-primary btn-sm">
			        </form>

			      </div>
			    </div>
			  </div>
			</div>
		@endforeach
	</div>
@endsection