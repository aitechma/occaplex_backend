

<p>Dear vendor!</p>
<p>You've received a new order on your Occaplex Vendor Platform for the listing titled <b>{{$title}}</b>.</p>
<br>
<p><b>Order ID:<b> {{$gref}}</p>
<p><b>Sales Price:</b> {{$sales_price}}</p>
<br>
<a href="https://www.occaplex.com/vendor/orders">Click here to visit your dashboard and fulfil the order.</a>

<p>Regards, <br><strong>Occaplex Vendor Support</strong></p>