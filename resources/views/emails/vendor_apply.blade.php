

<p>Dear {{$name}}!</p>
<p>We’ve received your vendor application to register as a vendor on occaplex. You will be notified of the status of your application within 48 hours. In the meantime explore <a href="https://occaplex.com">occaplex.com.</a></p>

<p>Regards, <br><strong>Occaplex Vendor Support</strong></p>