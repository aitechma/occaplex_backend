

<p>Dear {{$name}}!</p>
<p>Welcome to Occaplex, to complete your registration, kindly visit the link below to verify your email address.</p>

<p>{{$url}}</p>

<a href="{{$url}}"><button>Verify Email</button></a>