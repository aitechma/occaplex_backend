

<p>Dear customer!</p>
<p>Your order <b>{{$gref}}</b> has been submitted successfully</p>
<br>
<p><b>Order ID:<b> {{$gref}}</p>
<p><b>Sales Price:</b> {{$sales_price}}</p>
<p><b>Transaction Ref:</b> {{$payment_ref_id}}</p>
<br>
<a href="https://www.occaplex.com/account/orders">Click here to manage your orders.</a>

<p>Regards, <br><strong>Occaplex Vendor Support</strong></p>