

        <p>Dear {{$name}},</p>
        <p>You requested a password reset. Use the pin code below to complete your validation.</p>

        <p>{{$pin}}</p>